﻿using System.Collections.Generic;
using UnityEngine;

public class Teleportation : MonoBehaviour
{
    [SerializeField]
    protected Transform ovrCameraRig = default;

    [SerializeField]
    protected Transform playerHead = default;

    [SerializeField]
    protected Teleportation otherHandTeleportation = default;

    [SerializeField, LayerSelector]
    protected int raycastLayer = 0;

    [SerializeField]
    protected float velocity = 4f;

    [SerializeField]
    protected float maxCurveLength = 5;

    [SerializeField, TagSelector]    protected string floorTag = default;

    [SerializeField]
    protected Gradient validColorGradient = default;

    [SerializeField]
    protected Gradient invalidColorGradient = default;

    [SerializeField]
    protected GameObject teleportReticlePrefab = default;

    protected LineRenderer lineRenderer = default;

    protected GameObject teleportReticle = default;

    protected Vector3 lastHitPos = default;

    protected bool lastHitIsValid = default;

    protected float trajectoryVertDist = 0.01f;

    virtual protected void Start()
    {
        lineRenderer = GetComponent<LineRenderer>();

        teleportReticle = Instantiate(teleportReticlePrefab);
        teleportReticle.SetActive(false);
    }

    protected void DrawTrajectory(Vector3 direction)
    {
        var curvePoints = new List<Vector3> { transform.position };

        var currentPosition = transform.position;
        var currentVelocity = velocity * direction;

        RaycastHit hit;
        Ray ray = new Ray(currentPosition, currentVelocity.normalized);
        while (!Physics.Raycast(ray, out hit, trajectoryVertDist, LayerMask.GetMask(LayerMask.LayerToName(raycastLayer))) && Vector3.Distance(transform.position, currentPosition) < maxCurveLength)
        {
            var t = trajectoryVertDist / currentVelocity.magnitude;

            currentVelocity += t * Physics.gravity;
            currentPosition += t * currentVelocity;

            curvePoints.Add(currentPosition);

            ray = new Ray(currentPosition, currentVelocity.normalized);
        }

        if (hit.transform)
        {
            lastHitPos = hit.point;
            curvePoints.Add(hit.point);
            if (hit.transform.CompareTag(floorTag))
            {
                if (teleportReticle != null)
                {
                    teleportReticle.transform.position = hit.point;
                    teleportReticle.SetActive(true);
                }

                lastHitIsValid = true;
                lineRenderer.colorGradient = validColorGradient;
            }
            else
            {
                if (teleportReticle != null) teleportReticle.SetActive(false);

                lastHitIsValid = false;
                lineRenderer.colorGradient = invalidColorGradient;
            }
        }
        else
        {
            if (teleportReticle != null) teleportReticle.SetActive(false);

            lastHitIsValid = false;
            lineRenderer.colorGradient = invalidColorGradient;
        }

        lineRenderer.positionCount = curvePoints.Count;
        lineRenderer.SetPositions(curvePoints.ToArray());
    }

    protected void ClearTrajectory()
    {
        lineRenderer.positionCount = 0;
        if (teleportReticle != null) teleportReticle.SetActive(false);
    }

    protected void MovePlayerToLastHitPos()
    {
        Vector3 newPos = ovrCameraRig.position;
        newPos.x = lastHitPos.x + (ovrCameraRig.position.x - playerHead.position.x);
        newPos.z = lastHitPos.z + (ovrCameraRig.position.z - playerHead.position.z);
        ovrCameraRig.position = newPos;
    }

    protected void OnDisable()
    {
        ClearTrajectory();
    }
}
