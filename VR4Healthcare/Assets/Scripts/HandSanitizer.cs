﻿using OculusSampleFramework;
using UnityEngine;

public class HandSanitizer : MonoBehaviour
{
    [SerializeField]
    private Transform gelSpawner = null;

    public void OnPress()
    {
        if (GameManager.Instance.isPlaying)
        {
            Instantiate(Resources.Load("Prefabs/Gel Drop"), gelSpawner.position, gelSpawner.rotation);
        }
    }
}
