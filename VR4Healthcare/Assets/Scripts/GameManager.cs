﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public enum DifficultyLevel
    {
        Easy = 0,
        Normal = 1
    }

    public static GameManager Instance = default;

    public bool isPlaying = default;

    public bool isMappingEnabled = default;

    public DifficultyLevel difficultyLevel = DifficultyLevel.Normal;

    public bool areParticlesVisible = default;

    public bool readyToExitRoom = default;

    [HideInInspector]
    public Dictionary<string, HashSet<string>> contaminedObjects = new Dictionary<string, HashSet<string>>();

    [HideInInspector]
    public string playerName = "UNNAMED";

    [HideInInspector]
    public float durationSeconds = default;

    [HideInInspector]
    public Dictionary<string, bool> achievedGoals = new Dictionary<string, bool>(){
        { "DISINFECT_3_TIMES_BEFORE_PUNCTURE", false },
        { "WAIT_30_SECONDS_AFTER_DISINFECTION", true },
        { "NEEDLE_INSERTED_WITH_TOURNIQUET", true },
        { "BUTTERFLY_ATTACHED_FIRST_TRY", true },
        { "HEMOSTASIS_BEFORE_BLOOD_COUNT", false },
        { "NEEDLE_REMOVED_WITHOUT_TOURNIQUET", true },
        { "SPONGE_BLOOD_WITH_GAUZE", false },
        { "APPLY_BANDAGE", false },
        { "DISINFECTED_HANDS_BEFORE_GLOVING", false },
        { "GLOVES_IN_YELLOW_TRASH", false },
        { "SWABS_IN_BLACK_TRASH", false },
        { "GAUZES_IN_YELLOW_TRASH", false },
        { "BUTTERFLY_IN_SAFETY_BOX", false },
        { "PATIENT_NOT_INFECTED", false },
        { "CUPBOARD_NOT_INFECTED", false },
        { "USE_GLOVES_FOR_BLOOD_BODY_FLUIDS", true }
    };

    [SerializeField]
    private GameObject[] objectsToEnableOnStart = default;

    [SerializeField]
    private DoorHandle doorHandle = default;

    private void Awake()
    {
        Instance = this;
    }

    private void Start()
    {
        foreach (var obj in objectsToEnableOnStart) obj.SetActive(false);
        doorHandle.enabled = false;
        contaminedObjects.Add("Door Handle", new HashSet<string> { "self" });
    }

    public void StartScenario(bool isDifficultyEasy)
    {
        difficultyLevel = isDifficultyEasy ? DifficultyLevel.Easy : DifficultyLevel.Normal;
        if (difficultyLevel.Equals(DifficultyLevel.Easy)) areParticlesVisible = true;

        foreach (var obj in objectsToEnableOnStart) obj.SetActive(true);
        doorHandle.enabled = true;
        isPlaying = true;
    }

    public void ExitRoom(Action<bool> callback)
    {
        isPlaying = false;
        areParticlesVisible = true;
        foreach (var grabbable in FindObjectsOfType<OVRGrabbable>()) DestroyImmediate(grabbable);
        foreach (var trash in FindObjectsOfType<Trash>()) DestroyImmediate(trash);
        DestroyImmediate(FindObjectOfType<SafetyBox>());
        DestroyImmediate(FindObjectOfType<GlovesBox>());

        foreach (var handManager in FindObjectsOfType<HandManager>()) if (handManager.gloved) achievedGoals["GLOVES_IN_BLACK_TRASH"] = false;
        achievedGoals["SWABS_IN_BLACK_TRASH"] = achievedGoals["SWABS_IN_BLACK_TRASH"] && Swab.nbUsedSwabsRemaining == 0;
        achievedGoals["GAUZES_IN_YELLOW_TRASH"] = achievedGoals["GAUZES_IN_YELLOW_TRASH"] && Gauze.nbUsedGauzesRemaining == 0;
        achievedGoals["PATIENT_NOT_INFECTED"] = !contaminedObjects.ContainsKey("Patient");
        achievedGoals["CUPBOARD_NOT_INFECTED"] = !contaminedObjects.ContainsKey("Drawer 1") && !contaminedObjects.ContainsKey("Drawer 2") && !contaminedObjects.ContainsKey("Drawer 3") && !contaminedObjects.ContainsKey("Drawer 4");

        int gameScore = (int)(10 * 60 - durationSeconds);
        foreach (KeyValuePair<string, bool> achievedGoal in achievedGoals) if (achievedGoal.Value) gameScore += 100;

        var json = new
        {
            player_name = playerName,
            datetime = DateTime.Now.ToString("o"),
            duration = durationSeconds,
            score = gameScore,
            difficulty = (int)difficultyLevel,
            scenario = "BLOOD_TEST",
            achieved_goals = achievedGoals
        };
        string bodyJsonString = JsonConvert.SerializeObject(json);

        StartCoroutine(APIHelper.Instance.NewGame(bodyJsonString, callback));
    }
}
