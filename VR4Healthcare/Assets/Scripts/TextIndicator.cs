﻿using UnityEngine;

public class TextIndicator : MonoBehaviour
{
    [SerializeField, TagSelector]
    private string onProximityTag = null;

    [HideInInspector]
    public bool manualMode = false;

    private Canvas canvas = null;

    private bool hideCanvas = false;

    private void Start()
    {
        canvas = GetComponent<Canvas>();
    }

    private void Update()
    {
        if (hideCanvas && !manualMode)
        {
            hideCanvas = false;
            canvas.enabled = false;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag(onProximityTag))
        {
            if (!manualMode) canvas.enabled = true;
            else hideCanvas = false;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag(onProximityTag))
        {
            hideCanvas = true;
        }
    }
}
