﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class StartupMenu : MonoBehaviour
{
    [SerializeField]
    private GameObject canvas = default;

    [SerializeField]
    private GameObject keyboard = default;

    [SerializeField]
    private GameObject homePanel = default;

    [SerializeField]
    private GameObject homeKeys = default;

    [SerializeField]
    private GameObject online = default;

    [SerializeField]
    private GameObject offline = default;

    [SerializeField]
    private InputField usernameField = default;

    [SerializeField]
    private GameObject errorLabel = default;

    [SerializeField]
    private GameObject locomotionPanel = default;

    [SerializeField]
    private GameObject locomotionKeys = default;

    [SerializeField]
    private GameObject calibrationPanel = default;

    [SerializeField]
    private GameObject calibrationKeys = default;

    [SerializeField]
    private GameObject oculusBox = default;

    [SerializeField]
    private GameObject controlsPanel = default;

    [SerializeField]
    private GameObject controlsKeys = default;

    [SerializeField]
    private GameObject scenarioPanel = default;

    [SerializeField]
    private GameObject scenarioKeys = default;

    [SerializeField]
    private ControllerTeleportation[] controllerTeleportationInstances = default;

    [SerializeField]
    private HandTrackingTeleportation[] handTrackingTeleportationInstances = default;

    private bool checkServerState = false;

    private Action<bool> callback = default;

    private void Start()
    {
        callback = delegate (bool success)
        {
            if (success)
            {
                offline.SetActive(false);
                online.SetActive(true);
            }
            else
            {
                online.SetActive(false);
                offline.SetActive(true);
            }

            StartCoroutine(WaitThenCheckServerState());
        };

        checkServerState = true;
        CheckServerState();
    }

    IEnumerator WaitThenCheckServerState()
    {
        yield return new WaitForSeconds(1);
        CheckServerState();
    }

    private void CheckServerState()
    {
        if (checkServerState) StartCoroutine(APIHelper.Instance.CheckServerState(callback));
    }

    public void OnConfirmUsername()
    {
        if (usernameField.text.Length == 0)
        {
            errorLabel.SetActive(true);
            return;
        }

        checkServerState = false;
        GameManager.Instance.playerName = usernameField.text;

        errorLabel.SetActive(false);
        homeKeys.SetActive(false);
        homePanel.SetActive(false);
        locomotionPanel.SetActive(true);
        locomotionKeys.SetActive(true);
    }

    public void OnConfirmLocomotion(bool realWalking)
    {
        GameManager.Instance.isMappingEnabled = realWalking;

        locomotionKeys.SetActive(false);
        locomotionPanel.SetActive(false);
        if (realWalking)
        {
            calibrationPanel.SetActive(true);
            calibrationKeys.SetActive(true);
            oculusBox.SetActive(true);
        }
        else
        {
            controlsPanel.SetActive(true);
            controlsKeys.SetActive(true);
        }
    }

    public void OnConfirmCalibration()
    {
        foreach (var instance in controllerTeleportationInstances) instance.enabled = false;
        foreach (var instance in handTrackingTeleportationInstances) instance.enabled = false;
        oculusBox.SetActive(false);
        calibrationKeys.SetActive(false);
        calibrationPanel.SetActive(false);
        controlsPanel.SetActive(true);
        controlsKeys.SetActive(true);
    }

    public void OnConfirmControls()
    {
        controlsKeys.SetActive(false);
        controlsPanel.SetActive(false);
        scenarioPanel.SetActive(true);
        scenarioKeys.SetActive(true);
    }

    public void OnConfirmScenario(bool isDifficultyEasy)
    {
        keyboard.SetActive(false);
        canvas.SetActive(false);
        GameManager.Instance.StartScenario(isDifficultyEasy);
    }

    public void BackToHome()
    {
        locomotionKeys.SetActive(false);
        locomotionPanel.SetActive(false);
        homePanel.SetActive(true);
        homeKeys.SetActive(true);

        checkServerState = true;
        CheckServerState();
    }

    public void BackToLocomotionFromCalibration()
    {
        oculusBox.SetActive(false);
        calibrationKeys.SetActive(false);
        calibrationPanel.SetActive(false);
        locomotionPanel.SetActive(true);
        locomotionKeys.SetActive(true);
        foreach (var instance in controllerTeleportationInstances) instance.enabled = true;
        foreach (var instance in handTrackingTeleportationInstances) instance.enabled = true;
    }

    public void BackToLocomotionFromControls()
    {
        controlsKeys.SetActive(false);
        controlsPanel.SetActive(false);
        locomotionPanel.SetActive(true);
        locomotionKeys.SetActive(true);
        foreach (var instance in controllerTeleportationInstances) instance.enabled = true;
        foreach (var instance in handTrackingTeleportationInstances) instance.enabled = true;
    }

    public void BackToControlsFromScenario()
    {
        scenarioKeys.SetActive(false);
        scenarioPanel.SetActive(false);
        controlsPanel.SetActive(true);
        controlsKeys.SetActive(true);
    }
}
