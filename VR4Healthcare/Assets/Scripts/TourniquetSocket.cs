﻿using UnityEngine;

public class TourniquetSocket : Socket
{
    [SerializeField]
    private Patient patient = null;

    [SerializeField]
    private AudioClip onAttachSound = null;

    [SerializeField]
    private AudioClip onDetachSound = null;

    private AudioSource audioSource = null;

    private bool tourniquetShouldBeLocked = false;

    private bool tourniquetShouldBeUnlocked = false;

    private void Start()
    {
        audioSource = GetComponent<AudioSource>();
    }

    protected override void Update()
    {
        base.Update();

        if (tourniquetShouldBeLocked && attachedObject != null) attachedObject.GetComponent<SocketGrabbable>().allowOffhandGrab = false;
        else if (tourniquetShouldBeUnlocked && attachedObject != null) attachedObject.GetComponent<SocketGrabbable>().allowOffhandGrab = true;
    }

    public override void OnAttach()
    {
        audioSource.PlayOneShot(onAttachSound);
        patient.OnAttachTourniquet();
    }

    public override void OnDetach()
    {
        audioSource.PlayOneShot(onDetachSound);
        patient.OnDetachTourniquet();
    }

    public void LockTourniquet()
    {
        tourniquetShouldBeUnlocked = false;
        tourniquetShouldBeLocked = true;
    }

    public void UnlockTourniquet()
    {
        tourniquetShouldBeLocked = false;
        tourniquetShouldBeUnlocked = true;
    }
}
