﻿using System;using UnityEngine;public class Trash : MonoBehaviour{    private static bool swabOnceInTrash = default;    private static bool gauzeOnceInTrash = default;    private static bool gloveOnceInTrash = default;    [SerializeField, TagSelector]    private string[] correctGrabbableToDestroyTags = default;    [SerializeField, TagSelector]    private string[] wrongGrabbableToDestroyTags = default;    [SerializeField]    private AudioClip trashSound = default;    private ParticleSystem particles = default;    private AudioSource audioSource = default;    private void Start()    {        audioSource = GetComponent<AudioSource>();        particles = GetComponent<ParticleSystem>();        swabOnceInTrash = false;        gauzeOnceInTrash = false;        gloveOnceInTrash = false;    }    private void OnTriggerEnter(Collider other)    {
        bool correctTrash = Array.Exists(correctGrabbableToDestroyTags, element => other.CompareTag(element));
        bool wrongTrash = Array.Exists(wrongGrabbableToDestroyTags, element => other.CompareTag(element));

        if (correctTrash || wrongTrash)
        {
            if (other.CompareTag("Hand"))
            {
                HandManager handManager = other.GetComponent<HandManager>() ?? other.GetComponentInParent<HandManager>();
                if (handManager.gloved)
                {
                    audioSource.PlayOneShot(trashSound);
                    particles.Play();

                    if (!gloveOnceInTrash)
                    {
                        gloveOnceInTrash = true;
                        GameManager.Instance.achievedGoals["GLOVES_IN_YELLOW_TRASH"] = correctTrash;
                    }
                    else if (wrongTrash)
                    {
                        GameManager.Instance.achievedGoals["GLOVES_IN_YELLOW_TRASH"] = false;
                    }

                    handManager.TakeOffGlove();
                }
            }
            else if (!other.GetComponent<OVRGrabbable>().isGrabbed)
            {
                switch (other.tag)
                {
                    case "Swab":
                        if (!other.GetComponent<Swab>().isUsed) return;
                        Swab.nbUsedSwabsRemaining--;
                        if (!swabOnceInTrash)
                        {
                            swabOnceInTrash = true;
                            GameManager.Instance.achievedGoals["SWABS_IN_BLACK_TRASH"] = correctTrash;
                        } else if (wrongTrash)
                        {
                            GameManager.Instance.achievedGoals["SWABS_IN_BLACK_TRASH"] = false;
                        }
                        break;
                    case "Gauze":
                        if (!other.GetComponent<Gauze>().isUsed) return;
                        Gauze.nbUsedGauzesRemaining--;
                        if (!gauzeOnceInTrash)
                        {
                            gauzeOnceInTrash = true;
                            GameManager.Instance.achievedGoals["GAUZES_IN_YELLOW_TRASH"] = correctTrash;
                        }
                        else if (wrongTrash)
                        {
                            GameManager.Instance.achievedGoals["GAUZES_IN_YELLOW_TRASH"] = false;
                        }
                        break;
                    case "Butterfly":
                        if (!other.GetComponent<Butterfly>().canBeDestroyed) return;
                        break;
                }

                audioSource.PlayOneShot(trashSound);
                particles.Play();

                other.gameObject.GetComponent<Rigidbody>().detectCollisions = false;
                other.GetComponent<Rigidbody>().isKinematic = true;
                Destroy(other.gameObject, 0.1f);
            }
        }    }}