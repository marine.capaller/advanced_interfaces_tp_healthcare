﻿using UnityEngine;

public class GlovesBox : VRInteractable
{
    [SerializeField]
    private Material glovesMaterial = null;

    [SerializeField]
    private AudioClip putOnGloveSound = null;

    private AudioSource audioSource = null;

    private void Start()
    {
        audioSource = GetComponent<AudioSource>();
    }

    public override void OnGrab(GameObject hand)
    {
        HandManager handManager = hand.GetComponent<HandManager>() ?? hand.GetComponentInParent<HandManager>();
        if (!handManager.gloved)
        {
            audioSource.PlayOneShot(putOnGloveSound);
            handManager.PutOnGlove(glovesMaterial);
        }
    }
}
