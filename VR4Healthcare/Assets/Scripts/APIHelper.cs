﻿using System;
using System.Collections;
using System.Text;
using UnityEngine.Networking;

public sealed class APIHelper
{
    private static readonly object padlock = new object();

    private static APIHelper instance = null;

    public static APIHelper Instance
    {
        get
        {
            lock (padlock)
            {
                if (instance == null)
                {
                    instance = new APIHelper();
                }
                return instance;
            }
        }
    }

    private const string API_ADDRESS = "http://192.168.43.225:8000/api/";

    public APIHelper() { }

    public IEnumerator NewGame(string bodyJsonString, Action<bool> callback)
    {
        var request = new UnityWebRequest(API_ADDRESS + "new-game/", "POST");
        byte[] bodyRaw = Encoding.UTF8.GetBytes(bodyJsonString);
        request.uploadHandler = new UploadHandlerRaw(bodyRaw);
        request.downloadHandler = new DownloadHandlerBuffer();
        request.SetRequestHeader("Content-Type", "application/json");
        yield return request.SendWebRequest();
        callback.Invoke(request.responseCode == 200);
    }

    public IEnumerator CheckServerState(Action<bool> callback)
    {
        var request = UnityWebRequest.Get(API_ADDRESS + "check/");
        yield return request.SendWebRequest();
        callback.Invoke(request.responseCode == 200);
    }
}
