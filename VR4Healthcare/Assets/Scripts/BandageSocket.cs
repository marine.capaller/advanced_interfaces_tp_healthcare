﻿using UnityEngine;

public class BandageSocket : Socket
{
    [SerializeField]
    private Patient patient = null;

    [SerializeField]
    private AudioClip onAttachSound = null;

    private AudioSource audioSource = null;

    private void Start()
    {
        audioSource = GetComponent<AudioSource>();
    }

    public override void OnAttach()
    {
        patient.OnAttachBandage();
        audioSource.PlayOneShot(onAttachSound); 
        DestroyImmediate(attachedObject.GetComponent<OVRGrabbable>());
    }

    public override void OnDetach()
    {
        // This should never happen
    }
}