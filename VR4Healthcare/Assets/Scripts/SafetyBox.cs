﻿using UnityEngine;

public class SafetyBox : MonoBehaviour
{
    [SerializeField]
    private AudioClip trashSound = null;

    private AudioSource audioSource = null;

    private ParticleSystem particles = null;

    void Start()
    {
        audioSource = GetComponent<AudioSource>();
        particles = GetComponent<ParticleSystem>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Butterfly") && !other.GetComponent<OVRGrabbable>().isGrabbed && other.GetComponent<Butterfly>().canBeDestroyed)
        {
            GameManager.Instance.achievedGoals["BUTTERFLY_IN_SAFETY_BOX"] = true;
            audioSource.PlayOneShot(trashSound);
            particles.Play();

            other.GetComponent<Rigidbody>().detectCollisions = false;
            other.GetComponent<Rigidbody>().isKinematic = true;
            Destroy(other.gameObject, 0.1f);
        }
    }
}
