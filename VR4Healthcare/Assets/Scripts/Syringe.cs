﻿using UnityEngine;

public class Syringe : OVRGrabbable
{
    [SerializeField]
    private GameObject firstCap = default;

    [SerializeField]
    private Transform secondCap = default;

    [SerializeField]
    private Collider[] needleColliders = default;

    [SerializeField]
    private GameObject sticker = default;

    [SerializeField]
    private VacutainerSocket vacutainerSocket = default;

    [SerializeField]
    private int nbVacutainersToFill = 2;

    private bool isInserted = default;

    private bool isOpen = default;

    private int nbVacutainersFilled = 0;

    private void Update()
    {
        if (isGrabbed)
        {
            if (!isOpen)
            {
                isOpen = true;
                for (int i = 0; i < needleColliders.Length; i++) needleColliders[i].enabled = true;
                firstCap.SetActive(false);
                sticker.SetActive(false);
                secondCap.localEulerAngles = new Vector3(45, 0, 0);
            }

            if (vacutainerSocket.attachedObject != null) vacutainerSocket.attachedObject.GetComponent<Vacutainer>().allowOffhandGrab = true;
        } else
        {
            if (vacutainerSocket.attachedObject != null) vacutainerSocket.attachedObject.GetComponent<Vacutainer>().allowOffhandGrab = false;
        }
    }

    public Vacutainer GetAttachedVacutainer()
    {
        GameObject attachedObject = vacutainerSocket.attachedObject;
        return attachedObject != null ? attachedObject.GetComponent<Vacutainer>() : null;
    }

    public void OnVacutainerFilled()
    {
        nbVacutainersFilled++;
        if (!isInserted && nbVacutainersFilled >= nbVacutainersToFill)
        {
            for (int i = needleColliders.Length - 1; i >= 0; i--) DestroyImmediate(needleColliders[i]);
            secondCap.localEulerAngles = new Vector3(-90, 0, 0);
        }
    }

    public void OnInsert()
    {
        isInserted = true;
    }

    public void OnRemove()
    {
        isInserted = false;
        if (nbVacutainersFilled >= nbVacutainersToFill)
        {
            for (int i = needleColliders.Length - 1; i >= 0; i--) DestroyImmediate(needleColliders[i]);
            secondCap.localEulerAngles = new Vector3(-90, 0, 0);
        }
    }

    public bool CanBeDestroyed()
    {
        return nbVacutainersFilled >= nbVacutainersToFill;
    }
}
