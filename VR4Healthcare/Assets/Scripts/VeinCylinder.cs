﻿using UnityEngine;

public class VeinCylinder : MonoBehaviour
{
    [SerializeField]
    private VeinEntry veinEntry = null;

    private void OnTriggerEnter(Collider other)
    {
        veinEntry.OnEnterCylinder(other);
    }

    private void OnTriggerExit(Collider other)
    {
        veinEntry.OnExitCylinder(other);
    }
}
