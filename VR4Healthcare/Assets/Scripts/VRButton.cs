﻿using System;
using UnityEngine;
using UnityEngine.Events;

public class VRButton : MonoBehaviour
{
    public enum Axis
    {
        None,
        X,
        Y,
        Z
    }

    [SerializeField]
    private Axis axis = Axis.Y;

    [SerializeField]
    private float maxDisplacementDistance = -0.01f;

    [SerializeField]
    private float returnSpeed = 5f;

    [SerializeField]
    private UnityEvent onPress = default;

    [SerializeField]
    private UnityEvent onRelease = default;

    [SerializeField]
    private AudioClip onPressSound = default;

    [SerializeField]
    private AudioSource audioSource = default;

    [SerializeField]
    private bool enableHaptics = default;

    [SerializeField, TagSelector]
    private string leftControllerBoneTag = default;

    [SerializeField, TagSelector]
    private string rightControllerBoneTag = default;

    private Transform collidingBone = default;

    private Vector3 startPos = default;

    private Vector3 startLocalPos = default;

    private Vector3 lastPos = default;

    private bool isColliding = default;

    private float deltaToKeep = default;

    private bool invokeOnRelease = default;

    private void Start()
    {
        if (maxDisplacementDistance == 0) axis = Axis.None;

        startPos = transform.position;
        startLocalPos = transform.localPosition;
    }

    private void Update()
    {
        if (isColliding)
        {
            Vector3 buttonRelativeCollidingBonePos = transform.InverseTransformPoint(collidingBone.position);
            switch (axis)
            {
                case Axis.X:
                    float x = buttonRelativeCollidingBonePos.x - deltaToKeep;
                    if (maxDisplacementDistance > 0)
                    {
                        if (x > maxDisplacementDistance)
                        {
                            break;
                        }
                        else if (x > lastPos.x)
                        {
                            Vector3 pos = transform.localPosition;
                            pos.x = x;
                            transform.localPosition = pos;

                            lastPos = pos;
                        }
                    }
                    else if (maxDisplacementDistance < 0)
                    {
                        if (x < maxDisplacementDistance)
                        {
                            break;
                        }
                        else if (x < lastPos.x)
                        {
                            Vector3 pos = transform.localPosition;
                            pos.x = x;
                            transform.localPosition = pos;

                            lastPos = pos;
                        }
                    }
                    break;
                case Axis.Y:
                    float y = buttonRelativeCollidingBonePos.y - deltaToKeep;
                    if (maxDisplacementDistance > 0)
                    {
                        if (y > startLocalPos.y + maxDisplacementDistance)
                        {
                            break;
                        }
                        else if (y > lastPos.y)
                        {
                            Vector3 pos = transform.localPosition;
                            pos.y = y;
                            transform.localPosition = pos;

                            lastPos = pos;
                        }
                    }
                    else if (maxDisplacementDistance < 0)
                    {
                        if (y < startLocalPos.y + maxDisplacementDistance)
                        {
                            break;
                        }
                        else if (y < lastPos.y)
                        {
                            Vector3 pos = transform.localPosition;
                            pos.y = y;
                            transform.localPosition = pos;

                            lastPos = pos;
                        }
                    }
                    break;
                case Axis.Z:
                    float z = buttonRelativeCollidingBonePos.z - deltaToKeep;
                    if (maxDisplacementDistance > 0)
                    {
                        if (z > maxDisplacementDistance)
                        {
                            break;
                        }
                        else if (z > lastPos.z)
                        {
                            Vector3 pos = transform.localPosition;
                            pos.z = z;
                            transform.localPosition = pos;

                            lastPos = pos;
                        }
                    }
                    else if (maxDisplacementDistance < 0)
                    {
                        if (z < maxDisplacementDistance)
                        {
                            break;
                        }
                        else if (z < lastPos.z)
                        {
                            Vector3 pos = transform.localPosition;
                            pos.z = z;
                            transform.localPosition = pos;

                            lastPos = pos;
                        }
                    }
                    break;
            }
        }
        else if (transform.position != startPos)
        {
            transform.position = Vector3.Lerp(transform.position, startPos, Time.deltaTime * returnSpeed);

            bool lerpIsCloseEnough = false;
            switch (axis)
            {
                case Axis.X:
                    lerpIsCloseEnough = Math.Abs(transform.position.x - startPos.x) < 0.0001;
                    break;
                case Axis.Y:
                    lerpIsCloseEnough = Math.Abs(transform.position.y - startPos.y) < 0.0001;
                    break;
                case Axis.Z:
                    lerpIsCloseEnough = Math.Abs(transform.position.z - startPos.z) < 0.0001;
                    break;
            }

            if (lerpIsCloseEnough)
            {
                if (invokeOnRelease)
                {
                    invokeOnRelease = false;
                    onRelease.Invoke();
                }
                transform.position = startPos;
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (!isColliding)
        {
            isColliding = true;
            collidingBone = other.transform;

            switch (axis)
            {
                case Axis.X:
                    deltaToKeep = transform.InverseTransformPoint(other.transform.position).x - transform.localPosition.x;
                    break;
                case Axis.Y:
                    deltaToKeep = transform.InverseTransformPoint(other.transform.position).y - transform.localPosition.y;
                    break;
                case Axis.Z:
                    deltaToKeep = transform.InverseTransformPoint(other.transform.position).z - transform.localPosition.z;
                    break;
            }

            lastPos = transform.localPosition;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (isColliding) isColliding = false;
    }

    public void OnPress()
    {
        if (onRelease != null) invokeOnRelease = true;

        if (enableHaptics) SendHapticPulseToController();
        audioSource.PlayOneShot(onPressSound);

        onPress.Invoke();
    }

    private void SendHapticPulseToController()
    {
        if (collidingBone.CompareTag(leftControllerBoneTag)) OVRHaptics.LeftChannel.Preempt(new OVRHapticsClip(onPressSound));
        if (collidingBone.CompareTag(rightControllerBoneTag)) OVRHaptics.RightChannel.Preempt(new OVRHapticsClip(onPressSound));
    }

    private void OnDisable()
    {
        transform.localPosition = startLocalPos;
    }
}