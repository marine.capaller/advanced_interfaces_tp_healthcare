﻿using UnityEngine;

public class GauzeDistributor : MonoBehaviour
{
    [SerializeField]
    private Transform gauzeSpawner = null;

    private GameObject lastSpawnedGauze = null;

    private void Start()
    {
        SpawnGauze();
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Gauze") && other.gameObject.Equals(lastSpawnedGauze)) SpawnGauze();
    }

    private void SpawnGauze()
    {
        lastSpawnedGauze = (GameObject)Instantiate(Resources.Load("Prefabs/Gauze"), gauzeSpawner);
    }
}
