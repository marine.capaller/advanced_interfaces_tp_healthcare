﻿using UnityEngine;

public class HandParticles : MonoBehaviour
{
    [SerializeField]
    private HandManager handManager = default;

    [SerializeField]
    private ParticleSystem selfParticles = default;

    [SerializeField]
    private ParticleSystem otherParticles = default;

    private void Update()
    {
        if (GameManager.Instance.areParticlesVisible)
        {
            if (handManager.contaminations.Contains("self") && !selfParticles.isPlaying) selfParticles.Play();
            else if (!handManager.contaminations.Contains("self") && selfParticles.isPlaying) selfParticles.Stop();

            if (handManager.contaminations.Contains("other") && !otherParticles.isPlaying) otherParticles.Play();
            else if (!handManager.contaminations.Contains("other") && otherParticles.isPlaying) otherParticles.Stop();
        }
        else
        {
            if (selfParticles.isPlaying) selfParticles.Stop();
            if (otherParticles.isPlaying) otherParticles.Stop();
        }
    }
}
