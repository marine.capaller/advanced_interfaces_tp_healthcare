﻿using System.Collections.Generic;
using UnityEngine;
using static Vacutainer;

public class Butterfly : GenericGrabbable
{
    public int nbVacutainersToFill = 2;

    [HideInInspector]
    public List<Vacutainer> vacutainersFilled = default;

    [HideInInspector]
    public bool isAttached = default;

    [HideInInspector]
    public bool canBeDestroyed = default;

    [SerializeField]
    private GameObject cap = default;

    [SerializeField]
    private GameObject needle = default;

    [SerializeField]
    private GameObject sticker = default;

    [SerializeField]
    private VacutainerSocket vacutainerSocket = default;

    [SerializeField]
    private TourniquetSocket tourniquetSocket = default;

    private bool isOpen = default;

    private void Update()
    {
        if (isGrabbed)
        {
            if (!isOpen)
            {
                isOpen = true;
                cap.SetActive(false);
                sticker.SetActive(false);
            }
        }
    }

    public void OnAttach()
    {
        isAttached = true;
        vacutainerSocket.gameObject.SetActive(true);
        allowOffhandGrab = false;
        tourniquetSocket.LockTourniquet();
    }

    public void OnVacutainerFilled(Vacutainer vacutainer)
    {
        if (!vacutainersFilled.Contains(vacutainer))
        {
            vacutainersFilled.Add(vacutainer);
            if (vacutainersFilled.Count >= nbVacutainersToFill)
            {
                if (vacutainersFilled[0].vacutainerType.Equals(VacutainerType.Hemostasis) &&
                    vacutainersFilled[1].vacutainerType.Equals(VacutainerType.BloodCount))
                {
                    GameManager.Instance.achievedGoals["HEMOSTASIS_BEFORE_BLOOD_COUNT"] = true;
                }
                vacutainerSocket.gameObject.SetActive(false);
                allowOffhandGrab = true;
                tourniquetSocket.UnlockTourniquet();
            }
        }
    }

    public void HideNeedle()
    {
        needle.SetActive(false);
        canBeDestroyed = true;
    }
}
