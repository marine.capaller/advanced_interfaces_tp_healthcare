﻿using UnityEngine;
using UnityEngine.UI;

public class VisualTimer : MonoBehaviour
{
    [SerializeField]
    private Text timerText = null;

    [SerializeField]
    private float timerDuration = 30f;

    [SerializeField]
    private AudioClip timerSound = null;

    private VeinEntry veinEntry = default;

    private Canvas canvas = null;

    private AudioSource audioSource = null;

    private float timerStartTime = 0f;

    private void Start()
    {
        veinEntry = GetComponentInParent<VeinEntry>();
        canvas = GetComponent<Canvas>();
        audioSource = GetComponent<AudioSource>();
    }

    private void Update()
    {
        if (timerStartTime > 0)
        {
            Camera camera = Camera.current;
            if (camera != null)
            {
                transform.LookAt(camera.transform);
                transform.Rotate(0, 180, 0);
            }

            float timer = Time.time - timerStartTime;
            timerText.text = "" + (int)(timerDuration - timer);
            if (Time.time > timerStartTime + timerDuration)
            {
                audioSource.Stop();
                canvas.enabled = false;
                timerStartTime = 0f;
                veinEntry.OnTimerIsOver();
            }
        }
    }

    public void StartTimer()
    {
        audioSource.PlayOneShot(timerSound);
        timerStartTime = Time.time;
        canvas.enabled = true;
    }
}
