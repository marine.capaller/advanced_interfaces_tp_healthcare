﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class HandManager : MonoBehaviour
{
    public enum HandType
    {
        LeftHand,
        RightHand
    }

    public HandType handType = HandType.LeftHand;

    [HideInInspector]
    public HashSet<string> contaminations = new HashSet<string>();

    [HideInInspector]
    public bool gloved = default;

    private static bool glovedOnce = default;

    [SerializeField]
    private LocalAvatarManager localAvatarManager = default;

    [SerializeField, LayerSelector]
    private int bonesLayer = default;

    [SerializeField, TagSelector]
    private string[] infectableOnlyOnceTags = default;

    [SerializeField, TagSelector]
    private string[] infectableSeveralTimesTags = default;

    [SerializeField]
    private GameObject contaminationSitePrefab = default;

    [SerializeField]
    private Collider handTrackingCollider = default;

    [SerializeField]
    private Material handSkinMaterial = default;

    [SerializeField]
    private AudioClip disinfectSound = default;

    [SerializeField]
    private float lightnessAnimationFactor = 0.1f;

    [SerializeField]
    private int nbAnimationWaves = 2;

    [SerializeField]
    private float animationSpeed = 50;

    [SerializeField]
    private GameObject handTrackingParticles = default;

    [SerializeField]
    private GameObject controllerParticles = default;

    [SerializeField]
    private GameObject handTrackingHand = default;

    private AudioSource audioSource = default;

    private OVRMeshRenderer ovrMeshRenderer = default;

    private HashSet<string> contaminationsBeforeGloving = new HashSet<string>();

    private Collider controllerCollider = default;

    private bool bonesColliderLayerHasBeenSet = default;

    private bool animationIsRunning = default;

    private Color initialColor = default;

    private bool isLightnessIncreasing = true;

    private float currentLightnessFactor = default;

    private int currentAnimationWave = default;

    private void Start()
    {
        audioSource = GetComponent<AudioSource>();
        ovrMeshRenderer = handTrackingHand.GetComponent<OVRMeshRenderer>();
        controllerCollider = GetComponent<Collider>();

        initialColor = handSkinMaterial.color;
        if (handType.Equals(HandType.LeftHand))
        {
            localAvatarManager.leftHandManager = this;
            localAvatarManager.UpdateLeftHandMaterial(handSkinMaterial);
        }
        else
        {
            localAvatarManager.rightHandManager = this;
            localAvatarManager.UpdateRightHandMaterial(handSkinMaterial);
        }
        handTrackingHand.GetComponent<SkinnedMeshRenderer>().sharedMaterial = handSkinMaterial;

        glovedOnce = false;
        contaminations.Add("self");
    }

    private void Update()
    {
        // Set bones to specified layer
        if (!bonesColliderLayerHasBeenSet && handTrackingHand.transform.childCount > 1)
        {
            bonesColliderLayerHasBeenSet = true;
            SetLayerRecursively(handTrackingHand.transform.GetChild(3).gameObject);
        }

        // Controller
        bool isControllerActive = handType.Equals(HandType.LeftHand) ? localAvatarManager.isLeftControllerActive : localAvatarManager.isRightControllerActive;
        controllerCollider.enabled = isControllerActive;
        controllerParticles.SetActive(isControllerActive);

        // Hand tracking
        bool isHandTrackingActive = ovrMeshRenderer.IsDataHighConfidence;
        handTrackingCollider.enabled = isHandTrackingActive;
        handTrackingParticles.SetActive(isHandTrackingActive);

        // Disinfection animation
        if (animationIsRunning)
        {
            currentLightnessFactor += ((isLightnessIncreasing) ? 0.01f : -0.01f) * animationSpeed * Time.deltaTime;

            if (currentLightnessFactor > lightnessAnimationFactor)
            {
                currentLightnessFactor = lightnessAnimationFactor;
                isLightnessIncreasing = false;
            }
            else if (currentLightnessFactor <= 0f && !isLightnessIncreasing)
            {
                currentLightnessFactor = 0;
                isLightnessIncreasing = true;
                currentAnimationWave++;

                if (currentAnimationWave == nbAnimationWaves)
                {
                    currentAnimationWave = 0;
                    animationIsRunning = false;
                }
            }

            Color color = handSkinMaterial.color;
            color.r = initialColor.r / (1f - currentLightnessFactor);
            color.g = initialColor.g / (1f - currentLightnessFactor);
            color.b = initialColor.b / (1f - currentLightnessFactor);
            handSkinMaterial.color = color;
            handSkinMaterial.SetFloat("_Glossiness", currentLightnessFactor / lightnessAnimationFactor);
        }
    }

    private void SetLayerRecursively(GameObject obj)
    {
        obj.layer = bonesLayer;
        foreach (Transform child in obj.transform) SetLayerRecursively(child.gameObject);
    }

    public void Disinfect()
    {
        if (!gloved)
        {
            audioSource.PlayOneShot(disinfectSound);
            animationIsRunning = true;
            contaminations.Clear();
        }
    }

    public void PutOnGlove(Material glovesMaterial)
    {
        gloved = true;
        ovrMeshRenderer._originalMaterial = glovesMaterial;
        if (handType.Equals(HandType.LeftHand)) localAvatarManager.UpdateLeftHandMaterial(glovesMaterial);
        else localAvatarManager.UpdateRightHandMaterial(glovesMaterial);

        if (!glovedOnce)
        {
            glovedOnce = true;
            GameManager.Instance.achievedGoals["DISINFECTED_HANDS_BEFORE_GLOVING"] = contaminations.Count == 0;
        }
        else if (contaminations.Count > 0)
        {
            GameManager.Instance.achievedGoals["DISINFECTED_HANDS_BEFORE_GLOVING"] = false;
        }

        foreach (string contamination in contaminations)
        {
            contaminationsBeforeGloving.Add(contamination);
        }
    }

    public void TakeOffGlove()
    {
        gloved = false;
        ovrMeshRenderer._originalMaterial = handSkinMaterial;
        if (handType.Equals(HandType.LeftHand)) localAvatarManager.UpdateLeftHandMaterial(handSkinMaterial);
        else localAvatarManager.UpdateRightHandMaterial(handSkinMaterial);

        contaminations.Clear();
        foreach (string contamination in contaminationsBeforeGloving)
        {
            contaminations.Add(contamination);
        }
        contaminations.Add("self");
        contaminationsBeforeGloving.Clear();
    }

    public void OnTrigger(Collider other, Transform handTransform)
    {
        if (GameManager.Instance.isPlaying)
        {
            if (other.CompareTag("Contamination Site"))
            {
                contaminations.Add(other.gameObject.GetComponent<ContaminationSite>().source);
                return;
            }

            if (other.CompareTag("Patient")) contaminations.Add("other");

            bool isInfectableOnlyOnce = Array.Exists(infectableOnlyOnceTags, element => other.CompareTag(element));
            bool isInfectableSeveralTimes = Array.Exists(infectableSeveralTimesTags, element => other.CompareTag(element));
            if (isInfectableOnlyOnce || isInfectableSeveralTimes)
            {
                foreach (string contamination in contaminations)
                {
                    if (other.CompareTag("Patient") && contamination.Equals("other")) continue;

                    if (GameManager.Instance.contaminedObjects.ContainsKey(other.name))
                    {
                        if (!GameManager.Instance.contaminedObjects[other.name].Contains(contamination))
                        {
                            GameManager.Instance.contaminedObjects[other.name].Add(contamination);
                        }
                        else if (isInfectableOnlyOnce) continue;
                    }
                    else
                    {
                        GameManager.Instance.contaminedObjects.Add(other.name, new HashSet<string> { contamination });
                    }

                    Vector3 collisionPoint = other.ClosestPointOnBounds(handTransform.position);

                    // Fix collision point position for hand tracking
                    if (collisionPoint == transform.position && transform != handTransform)
                    {
                        if (handType.Equals(HandType.LeftHand)) collisionPoint = transform.TransformPoint(new Vector3(0.1f, 0.05f));
                        else collisionPoint = transform.TransformPoint(new Vector3(-0.1f, -0.05f));
                    }

                    GameObject contaminationSite = Instantiate(contaminationSitePrefab, collisionPoint, Quaternion.identity, other.transform);

                    switch (contamination)
                    {
                        case "self":
                            contaminationSite.GetComponent<ParticleSystemRenderer>().material = Resources.Load<Material>("Materials/SelfParticle");
                            break;
                        case "other":
                            contaminationSite.GetComponent<ParticleSystemRenderer>().material = Resources.Load<Material>("Materials/OtherParticle");
                            break;
                    }

                    contaminationSite.GetComponent<ContaminationSite>().source = contamination;
                }

            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        OnTrigger(other, transform);
    }
}
