﻿using UnityEngine;

public class VeinEntry : MonoBehaviour
{
    [HideInInspector]
    public bool lookingForVacutainer = default;

    [HideInInspector]
    public bool isNeedleInsideEntry = default;

    [HideInInspector]
    public bool isNeedleInsideCylinder = default;

    [SerializeField]
    private Patient patient = default;

    [SerializeField]
    private Transform butterflySocket = default;

    [SerializeField]
    private BandageSocket bandageSocket = default;

    [SerializeField]
    private MeshRenderer veinRenderer = default;

    [SerializeField]
    private Material validVeinMaterial = default;

    [SerializeField]
    private VisualTimer visualTimer = default;

    [SerializeField]
    private AudioClip[] disinfectionSounds = default;

    [SerializeField]
    private AudioClip bloodAbsorptionSound = default;

    private AudioSource audioSource = default;

    private Butterfly butterfly = default;

    private Material veinMaterial = default;

    private bool isButterflyAttached = default;

    private bool butterflyHasBeenAttached = default;

    private bool timerIsOver = default;

    private void Start()
    {
        veinMaterial = veinRenderer.material;
        audioSource = GetComponent<AudioSource>();
    }

    private void Update()
    {
        if (!isButterflyAttached && butterfly != null && !butterfly.isGrabbed && butterfly.vacutainersFilled.Count < butterfly.nbVacutainersToFill)
        {
            if (!butterflyHasBeenAttached) butterflyHasBeenAttached = true;
            isButterflyAttached = true;

            butterfly.transform.parent = butterflySocket;
            butterfly.GetComponent<Rigidbody>().isKinematic = true;
            butterfly.transform.position = butterflySocket.position;
            butterfly.transform.rotation = butterflySocket.rotation;

            butterfly.OnAttach();
        }
        else if (isButterflyAttached && butterfly.isGrabbed)
        {
            isButterflyAttached = false;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (!butterflyHasBeenAttached && other.CompareTag("Swab") && !patient.animationIsRunning)
        {
            Swab swab = other.GetComponent<Swab>();
            if (!swab.isUsed && other.GetComponent<OVRGrabbable>().isGrabbed == true)
            {
                swab.Use();
                patient.disinfectionCounter++;
                switch (patient.disinfectionCounter)
                {
                    case 1:
                        audioSource.PlayOneShot(disinfectionSounds[patient.disinfectionCounter - 1]);
                        break;
                    case 2:
                        audioSource.PlayOneShot(disinfectionSounds[patient.disinfectionCounter - 1]);
                        break;
                    case 3:
                        audioSource.PlayOneShot(disinfectionSounds[patient.disinfectionCounter - 1]);
                        visualTimer.StartTimer();
                        GameManager.Instance.achievedGoals["DISINFECT_3_TIMES_BEFORE_PUNCTURE"] = true;
                        break;
                }

                patient.OnDisinfectArea();
            }
        }

        if (!isButterflyAttached && other.CompareTag("Needle 1"))
        {
            isNeedleInsideEntry = true;

            if (isNeedleInsideCylinder)
            {
                butterfly = other.GetComponentInParent<Butterfly>();
                lookingForVacutainer = true;
                veinRenderer.material = validVeinMaterial;
                if (!timerIsOver) GameManager.Instance.achievedGoals["WAIT_30_SECONDS_AFTER_DISINFECTION"] = false;
                patient.OnNeedleInserted();
            }
        }

        if (!isButterflyAttached && patient.isBlood && other.CompareTag("Gauze"))
        {
            Gauze gauze = other.GetComponent<Gauze>();
            if (!gauze.isUsed && other.GetComponent<OVRGrabbable>().isGrabbed == true)
            {
                gauze.Use();
                audioSource.PlayOneShot(bloodAbsorptionSound);
                patient.OnAbsorBlood();
            }
        }
    }

    public void OnEnterCylinder(Collider other)
    {
        if (!isButterflyAttached && other.CompareTag("Needle 2"))
        {
            isNeedleInsideCylinder = true;

            if (isNeedleInsideEntry)
            {
                butterfly = other.GetComponentInParent<Butterfly>();
                lookingForVacutainer = true;
                veinRenderer.material = validVeinMaterial;
                if (!timerIsOver) GameManager.Instance.achievedGoals["WAIT_30_SECONDS_AFTER_DISINFECTION"] = false;
                patient.OnNeedleInserted();
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (!isButterflyAttached && other.CompareTag("Needle 1"))
        {
            isNeedleInsideEntry = false;
            veinRenderer.material = veinMaterial;

            if (isNeedleInsideCylinder)
            {
                if (!butterflyHasBeenAttached) GameManager.Instance.achievedGoals["BUTTERFLY_ATTACHED_FIRST_TRY"] = false;
                else if (patient.isTourniquetAttached) GameManager.Instance.achievedGoals["NEEDLE_REMOVED_WITHOUT_TOURNIQUET"] = false;

                patient.OnNeedleRemoved();

                if (butterflyHasBeenAttached)
                {
                    GameManager.Instance.readyToExitRoom = true;
                    butterfly.HideNeedle();
                    bandageSocket.enabled = true;
                }

                butterfly = null;
            }
        }
    }

    public void OnExitCylinder(Collider other)
    {
        if (!isButterflyAttached && other.CompareTag("Needle 2"))
        {
            isNeedleInsideCylinder = false;
            veinRenderer.material = veinMaterial;

            if (isNeedleInsideEntry)
            {
                if (!butterflyHasBeenAttached) GameManager.Instance.achievedGoals["BUTTERFLY_ATTACHED_FIRST_TRY"] = false;
                else if (patient.isTourniquetAttached) GameManager.Instance.achievedGoals["NEEDLE_REMOVED_WITHOUT_TOURNIQUET"] = false;

                patient.OnNeedleRemoved();

                if (butterflyHasBeenAttached)
                {
                    GameManager.Instance.readyToExitRoom = true;
                    butterfly.HideNeedle();
                    bandageSocket.enabled = true;
                }

                butterfly = null;
            }
        }
    }

    public void OnTimerIsOver()
    {
        timerIsOver = true;
    }
}
