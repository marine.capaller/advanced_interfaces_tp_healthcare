﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using static OVRSkeleton;

public class HandTrackingTeleportation : Teleportation
{
    [SerializeField]
    private List<Vector3> fingersData = default;

    [SerializeField]
    private float threshold = 0.05f;

    private OVRSkeleton ovrSkeleton = default;

    private List<OVRBone> fingerBones = default;

    private SkinnedMeshRenderer meshRenderer = default;

    private bool teleportationMode = default;

    protected override void Start()
    {
        base.Start();

        ovrSkeleton = GetComponent<OVRSkeleton>();
        meshRenderer = GetComponent<SkinnedMeshRenderer>();
    }

    private void Update()
    {
        if (fingerBones == null)
        {
            if (ovrSkeleton.Bones.Count > 0) fingerBones = ovrSkeleton.Bones.ToList();
            else return;
        }

        if (!meshRenderer.enabled)
        {
            ClearTrajectory();
            if (teleportReticle != null) teleportReticle.SetActive(false);
            otherHandTeleportation.enabled = true;
            return;
        }

        bool recognized = Recognize();

        if (!recognized && teleportationMode)
        {
            if (teleportReticle != null) teleportReticle.SetActive(false);
            ClearTrajectory();
        }

        teleportationMode = recognized;

        if (teleportationMode)
        {
            Vector3 direction = ovrSkeleton.GetSkeletonType().Equals(SkeletonType.HandLeft) ? transform.right : -transform.right;
            DrawTrajectory(direction);
        }
    }

    private bool Recognize()
    {
        if (ovrSkeleton.GetSkeletonType().Equals(SkeletonType.HandLeft))
        {
            if (transform.eulerAngles.z < 290 && transform.eulerAngles.z > 45) return false;
        }
        else
        {
            if (transform.eulerAngles.z < 110 || transform.eulerAngles.z > 225) return false;
        }

        for (var j = 0; j < fingerBones.Count; j++)
        {
            var fingerRelativePos = transform.InverseTransformPoint(fingerBones[j].Transform.position);

            if (Vector3.Distance(fingerRelativePos, fingersData[j]) > threshold)
            {
                if ((j == 8 || j == 20) && teleportationMode && lastHitIsValid) MovePlayerToLastHitPos();

                otherHandTeleportation.enabled = true;
                return false;
            }
        }

        otherHandTeleportation.enabled = false;
        return true;
    }
}
