﻿
using UnityEngine;

public class ContaminationSite : MonoBehaviour
{
    public string source = default;

    private ParticleSystem particles = default;

    void Start()
    {
        particles = GetComponent<ParticleSystem>();
    }

    void Update()
    {
        if (GameManager.Instance.areParticlesVisible && particles.isStopped) particles.Play();
        else if (!GameManager.Instance.areParticlesVisible && particles.isPlaying) particles.Stop();
    }
}
