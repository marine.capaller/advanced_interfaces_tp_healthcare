﻿using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UnityEngine.Events;
using System;
#if UNITY_EDITOR
using UnityEditor;
#endif

[Serializable]
public struct Gesture
{
    public string name;
    public List<Vector3> fingersData;
    public UnityEvent onRecognized;
}

public class GestureRecognizer : MonoBehaviour
{
    [Header("Behaviour")]

    [SerializeField]
    private List<Gesture> savedGestures = new List<Gesture>();

    [SerializeField]
    private float threshold = 0.05f;

    [SerializeField]
    private UnityEvent onNothingDetected = default;

    [Header("Debugging")]

    [SerializeField]
    private Gesture gestureDetected = default;

    private OVRSkeleton ovrSkeleton = default;

    private List<OVRBone> fingerBones = default;

    private bool sthWasDetected;

    private void Start()
    {
        ovrSkeleton = GetComponent<OVRSkeleton>();

        sthWasDetected = false;
        onNothingDetected.Invoke();
    }

    private void Update()
    {
        if (fingerBones == null)
        {
            if (ovrSkeleton.Bones.Count > 0) fingerBones = ovrSkeleton.Bones.ToList();
            else return;
        }

        gestureDetected = Recognize();

        if (gestureDetected.Equals(new Gesture()) && sthWasDetected)
        {
            sthWasDetected = false;
            onNothingDetected.Invoke();
        }
        else if (!gestureDetected.Equals(new Gesture()))
        {
            sthWasDetected = true;
            if (gestureDetected.onRecognized != null) gestureDetected.onRecognized.Invoke();
        }
    }

    public void SaveAsGesture()
    {
        var g = new Gesture { name = "New gesture" };
        var positions = fingerBones.Select(t => transform.InverseTransformPoint(t.Transform.position)).ToList();
        g.fingersData = positions;
        savedGestures.Add(g);
    }

    private Gesture Recognize()
    {
        var discardGesture = false;
        var minSumDistances = Mathf.Infinity;
        var bestCandidate = new Gesture();

        for (var i = 0; i < savedGestures.Count; i++)
        {
            if (fingerBones.Count != savedGestures[i].fingersData.Count)
                throw new Exception("Different number of tracked fingers");

            var sumDistances = 0f;

            for (var j = 0; j < fingerBones.Count; j++)
            {
                var fingerRelativePos = transform.InverseTransformPoint(fingerBones[j].Transform.position);

                if (Vector3.Distance(fingerRelativePos, savedGestures[i].fingersData[j]) > threshold)
                {
                    discardGesture = true;
                    break;
                }

                sumDistances += Vector3.Distance(fingerRelativePos, savedGestures[i].fingersData[j]);
            }

            if (discardGesture)
            {
                discardGesture = false;
                continue;
            }

            if (sumDistances < minSumDistances)
            {
                minSumDistances = sumDistances;
                bestCandidate = savedGestures[i];
            }
        }

        return bestCandidate;
    }
}

#if UNITY_EDITOR
[CustomEditor(typeof(GestureRecognizer))]
public class CustomInspectorGestureRecognizer : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();
        var gestureRecognizer = (GestureRecognizer)target;
        if (!GUILayout.Button("Save current gesture")) return;
        gestureRecognizer.SaveAsGesture();
    }
}
#endif