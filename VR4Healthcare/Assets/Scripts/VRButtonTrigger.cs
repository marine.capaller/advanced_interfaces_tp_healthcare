﻿using UnityEngine;

public class VRButtonTrigger : MonoBehaviour
{
    [SerializeField]
    private VRButton vrButton = default;

    [SerializeField]
    private GameObject button = default;

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.Equals(button)) vrButton.OnPress();
    }
}