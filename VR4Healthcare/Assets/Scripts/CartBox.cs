﻿using UnityEngine;

public class CartBox : MonoBehaviour
{
    [SerializeField]
    private AudioClip cartBoxSound = null;

    [SerializeField]
    private Canvas canvas = null;

    private Animator animator = null;

    private AudioSource audioSource = null;

    private bool animationIsRunning = false;

    private bool opened = false;

    private void Start()
    {
        animator = GetComponent<Animator>();
        audioSource = GetComponent<AudioSource>();
    }

    public void OpenClose()
    {
        if (!animationIsRunning)
        {
            audioSource.PlayOneShot(cartBoxSound);
            animationIsRunning = true;
            canvas.GetComponent<TextIndicator>().manualMode = true;
            opened = !opened;
            canvas.enabled = false;
            animator.SetBool("opened", opened);
        }
    }

    public void OnAnimationExit()
    {
        canvas.enabled = true;
        animationIsRunning = false;
        canvas.GetComponent<TextIndicator>().manualMode = false;
    }
}
