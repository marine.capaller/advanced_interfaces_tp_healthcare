﻿using UnityEngine;

public class VacutainerSocket : Socket
{
    [SerializeField, LayerSelector]
    private int hubAttachedModeLayer = default;

    [SerializeField, LayerSelector]
    private int vacutainerAttachedModeLayer = default;

    [SerializeField]
    private GameObject hub = default;

    [SerializeField]
    private Material tubeMaterial = default;

    [SerializeField]
    private Material bloodMaterial = default;

    private Butterfly butterfly = default;

    private int originalHubLayer = default;

    private int originalVacutainerLayer = default;

    private void Start()
    {
        butterfly = GetComponentInParent<Butterfly>();
    }

    public override void OnAttach()
    {
        Vacutainer vacutainer = attachedObject.GetComponent<Vacutainer>();
        vacutainer.OnAttach();

        originalHubLayer = hub.layer;
        originalVacutainerLayer = attachedObject.layer;

        hub.layer = hubAttachedModeLayer;
        attachedObject.layer = vacutainerAttachedModeLayer;

        if (!vacutainer.isFilled)
        {
            vacutainer.isFillingUp = true;
            vacutainer.allowOffhandGrab = false;
            MeshRenderer hubRenderer = hub.GetComponent<MeshRenderer>();
            Material[] materials = hubRenderer.materials;
            materials[3] = bloodMaterial;
            hubRenderer.materials = materials;
        }
    }

    public override void OnDetach()
    {
        Vacutainer vacutainer = attachedObject.GetComponent<Vacutainer>();
        vacutainer.OnDetach();

        if (vacutainer.isFilled) butterfly.OnVacutainerFilled(vacutainer);

        hub.layer = originalHubLayer;
        attachedObject.layer = originalVacutainerLayer;

        MeshRenderer hubRenderer = hub.GetComponent<MeshRenderer>();
        Material[] materials = hubRenderer.materials;
        materials[3] = tubeMaterial;
        hubRenderer.materials = materials;
    }
}
