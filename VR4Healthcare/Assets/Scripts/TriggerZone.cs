﻿using UnityEngine;
using UnityEngine.Events;

public class TriggerZone : MonoBehaviour
{
    [SerializeField, TagSelector]
    private string triggeredTag = null;

    [SerializeField]
    private UnityEvent onTriggerEnter = null;

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag(triggeredTag)) onTriggerEnter.Invoke();
    }
}
