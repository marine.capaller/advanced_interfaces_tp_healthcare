﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;

public abstract class VRInteractable : MonoBehaviour
{
    [SerializeField, TagSelector]
    private string handTag = null;

    private HashSet<OVRGrabber> candidateHands = new HashSet<OVRGrabber>();

    private void Update()
    {
        List<OVRGrabber> candidatesToRemove = new List<OVRGrabber>();
        foreach (var candidateHand in candidateHands.Reverse())
        {
            if (candidateHand.isGrabbing)
            {
                candidatesToRemove.Add(candidateHand);
                OnGrab(candidateHand.gameObject);
            }
        }

        foreach (var candidate in candidatesToRemove) candidateHands.Remove(candidate);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag(handTag)) candidateHands.Add(other.GetComponent<OVRGrabber>());
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag(handTag)) candidateHands.Remove(other.GetComponent<OVRGrabber>());
    }

    public abstract void OnGrab(GameObject hand);
}
