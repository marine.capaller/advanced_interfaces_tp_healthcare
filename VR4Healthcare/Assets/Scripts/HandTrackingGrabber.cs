﻿using UnityEngine;
using static OVRHand;

public class HandTrackingGrabber : OVRGrabber
{
    [SerializeField]
    private float pinchThreshold = 0.7f;

    private OVRHand hand = default;

    private Vector3 lastPosition = default;

    private Vector3 lastRotation = default;

    protected override void Start()
    {
        base.Start();

        hand = GetComponent<OVRHand>();

        lastPosition = transform.position;
        lastRotation = transform.eulerAngles;
    }

    public override void Update()
    {
        base.Update();

        CheckIndexPinch();

        lastPosition = transform.position;
        lastRotation = transform.eulerAngles;
    }

    private void CheckIndexPinch()
    {
        float pinchStrength = hand.GetFingerPinchStrength(HandFinger.Index);
        isGrabbing = pinchStrength > pinchThreshold;

        if (!m_grabbedObj && isGrabbing) GrabBegin();
        else if (m_grabbedObj && !isGrabbing) GrabEnd();
    }

    protected override void GrabBegin()
    {
        float closestMagSq = float.MaxValue;
        OVRGrabbable closestGrabbable = null;
        Collider closestGrabbableCollider = null;

        // Iterate grab candidates and find the closest grabbable candidate
        foreach (OVRGrabbable grabbable in m_grabCandidates.Keys)
        {
            bool canGrab = !grabbable.isGrabbed && grabbable.allowOffhandGrab;
            if (!canGrab)
            {
                continue;
            }

            for (int j = 0; j < grabbable.grabPoints.Length; ++j)
            {
                Collider grabbableCollider = grabbable.grabPoints[j];
                // Store the closest grabbable
                Vector3 closestPointOnBounds = grabbableCollider.ClosestPointOnBounds(m_gripTransform.position);
                float grabbableMagSq = (m_gripTransform.position - closestPointOnBounds).sqrMagnitude;
                if (grabbableMagSq < closestMagSq)
                {
                    closestMagSq = grabbableMagSq;
                    closestGrabbable = grabbable;
                    closestGrabbableCollider = grabbableCollider;
                }
            }
        }

        // Disable grab volumes to prevent overlaps
        GrabVolumeEnable(false);

        if (closestGrabbable != null)
        {
            if (closestGrabbable.isGrabbed)
            {
                HandTrackingGrabber grabbedBy = (HandTrackingGrabber)closestGrabbable.grabbedBy;
                grabbedBy.OffhandGrabbed(closestGrabbable);
            }

            m_grabbedObj = closestGrabbable;
            m_grabbedObj.GrabBegin(this, closestGrabbableCollider);

            m_lastPos = transform.position;
            m_lastRot = transform.rotation;

            // Set up offsets for grabbed object desired position relative to hand.
            GenericGrabbable genericGrabbable = m_grabbedObj.GetComponent<GenericGrabbable>();
            if (m_grabbedObj.snapPosition && genericGrabbable)
            {
                if (m_controller == OVRInput.Controller.LHand)
                {
                    Transform handTrackingSnapOffset = genericGrabbable.leftHandTrackingSnapOffset;
                    if (handTrackingSnapOffset)
                    {
                        m_grabbedObjectPosOff = handTrackingSnapOffset.localPosition;
                    }
                    else
                    {
                        m_grabbedObjectPosOff = Vector3.zero;
                    }
                }
                else
                {
                    Transform handTrackingSnapOffset = genericGrabbable.rightHandTrackingSnapOffset;
                    if (handTrackingSnapOffset)
                    {
                        m_grabbedObjectPosOff = handTrackingSnapOffset.localPosition;
                    }
                    else
                    {
                        m_grabbedObjectPosOff = Vector3.zero;
                    }
                }
            }
            else
            {
                Vector3 relPos = m_grabbedObj.transform.position - transform.position;
                relPos = Quaternion.Inverse(transform.rotation) * relPos;
                m_grabbedObjectPosOff = relPos;
            }

            if (m_grabbedObj.snapOrientation && genericGrabbable)
            {
                if (m_controller == OVRInput.Controller.LHand)
                {
                    Transform handTrackingSnapOffset = genericGrabbable.leftHandTrackingSnapOffset;
                    if (handTrackingSnapOffset) m_grabbedObjectRotOff = handTrackingSnapOffset.localRotation;
                    else m_grabbedObjectRotOff = Quaternion.identity;
                }
                else
                {
                    Transform handTrackingSnapOffset = genericGrabbable.rightHandTrackingSnapOffset;
                    if (handTrackingSnapOffset) m_grabbedObjectRotOff = handTrackingSnapOffset.localRotation;
                    else m_grabbedObjectRotOff = Quaternion.identity;
                }
            }
            else
            {
                Quaternion relOri = Quaternion.Inverse(transform.rotation) * m_grabbedObj.transform.rotation;
                m_grabbedObjectRotOff = relOri;
            }

            // NOTE: force teleport on grab, to avoid high-speed travel to dest which hits a lot of other objects at high
            // speed and sends them flying. The grabbed object may still teleport inside of other objects, but fixing that
            // is beyond the scope of this demo.
            MoveGrabbedObject(m_lastPos, m_lastRot, true);

            // NOTE: This is to get around having to setup collision layers, but in your own project you might
            // choose to remove this line in favor of your own collision layer setup.
            SetPlayerIgnoreCollision(m_grabbedObj.gameObject, true);

            if (m_parentHeldObject)
            {
                m_grabbedObj.transform.parent = transform;
            }
        }
    }

    protected override void GrabEnd()
    {
        if (m_grabbedObj != null)
        {
            Vector3 linearVelocity = (transform.position - lastPosition) / Time.fixedDeltaTime;
            Vector3 angularVelocity = (transform.eulerAngles - lastRotation) / Time.fixedDeltaTime;

            GrabbableRelease(linearVelocity, angularVelocity);
        }

        GrabVolumeEnable(true);
    }
}
