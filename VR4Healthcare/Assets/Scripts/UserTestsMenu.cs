﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class UserTestsMenu : MonoBehaviour
{
    public void StartScene(int i)
    {
        switch (i)
        {
            case 0:
                SceneManager.LoadScene("V_H");
                break;
            case 1:
                SceneManager.LoadScene("V_C");
                break;
            case 2:
                SceneManager.LoadScene("I_H");
                break;
            case 3:
                SceneManager.LoadScene("I_C");
                break;
            case 4:
                SceneManager.LoadScene("M_C");
                break;
            case 5:
                SceneManager.LoadScene("M_P");
                break;

        }
    }
}
