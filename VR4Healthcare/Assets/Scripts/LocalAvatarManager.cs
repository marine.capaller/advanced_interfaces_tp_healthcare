﻿using UnityEngine;

public class LocalAvatarManager : MonoBehaviour
{
    public enum VisualizationMode
    {
        HandsOnly,
        HandsAndControllers,
        ControllersOnly
    }

    public VisualizationMode leftVisualizationMode = default;

    public VisualizationMode rightVisualizationMode = default;

    [HideInInspector]
    public HandManager leftHandManager = default;

    [HideInInspector]
    public HandManager rightHandManager = default;

    [HideInInspector]
    public bool isLeftControllerActive = default;

    [HideInInspector]
    public bool isRightControllerActive = default;

    [SerializeField, LayerSelector]
    private int bonesLayer = default;

    [SerializeField, TagSelector]
    private string leftControllerBoneTag = default;

    [SerializeField, TagSelector]
    private string rightControllerBoneTag = default;

    [SerializeField]
    private bool enablePhysicsColliders = default;

    private OvrAvatar avatar = default;

    private Material leftHandMaterial = default;

    private Material rightHandMaterial = default;

    private bool leftHandMaterialHasBeenSet = default;

    private bool rightHandMaterialHasBeenSet = default;

    private bool leftHandCollidersHasBeenSet = default;

    private bool rightHandCollidersHasBeenSet = default;

    private bool leftVisualizationModeHasBeenSet = default;

    private bool rightVisualizationModeHasBeenSet = default;

    private void Start()
    {
        avatar = GetComponent<OvrAvatar>();
    }

    private void Update()
    {
        // Left visualization mode
        if (!leftVisualizationModeHasBeenSet && avatar.HandLeft != null)
        {
            leftVisualizationModeHasBeenSet = true;
            SwitchLeftControllerVisualization(leftVisualizationMode);
        }

        // Right visualization mode
        if (!rightVisualizationModeHasBeenSet && avatar.HandRight != null)
        {
            rightVisualizationModeHasBeenSet = true;
            SwitchRightControllerVisualization(rightVisualizationMode);
        }

        // Hands colliders
        if (enablePhysicsColliders)
        {
            if (!leftHandCollidersHasBeenSet && transform.childCount > 0 && transform.GetChild(0).childCount > 0)
            {
                Transform hand = transform.GetChild(0).GetChild(0).GetChild(0).GetChild(0);

                for (int i = 2; i < hand.childCount; i++)
                {
                    Transform actualNode = hand.GetChild(i);
                    while (actualNode.childCount != 0) actualNode = actualNode.GetChild(0);

                    actualNode.gameObject.layer = bonesLayer;
                    actualNode.tag = leftControllerBoneTag;
                    SphereCollider collider = actualNode.gameObject.AddComponent(typeof(SphereCollider)) as SphereCollider;
                    collider.radius = 0.01f;
                    collider.center = new Vector3(0.01f, 0, 0);
                    Rigidbody rigidbody = actualNode.gameObject.AddComponent(typeof(Rigidbody)) as Rigidbody;
                    rigidbody.isKinematic = true;
                    rigidbody.collisionDetectionMode = CollisionDetectionMode.ContinuousSpeculative;
                }

                Instantiate(Resources.Load("Prefabs/Left Hand Palm Capsules"), hand);

                leftHandCollidersHasBeenSet = true;
            }

            if (!rightHandCollidersHasBeenSet && transform.childCount > 0 && transform.GetChild(1).childCount > 0)
            {
                Transform hand = transform.GetChild(1).GetChild(0).GetChild(0).GetChild(0);

                for (int i = 2; i < hand.childCount; i++)
                {
                    Transform actualNode = hand.GetChild(i);
                    while (actualNode.childCount != 0) actualNode = actualNode.GetChild(0);

                    actualNode.gameObject.layer = bonesLayer;
                    actualNode.tag = rightControllerBoneTag;
                    SphereCollider collider = actualNode.gameObject.AddComponent(typeof(SphereCollider)) as SphereCollider;
                    collider.radius = 0.01f;
                    collider.center = new Vector3(-0.01f, 0, 0);
                    Rigidbody rigidbody = actualNode.gameObject.AddComponent(typeof(Rigidbody)) as Rigidbody;
                    rigidbody.isKinematic = true;
                    rigidbody.collisionDetectionMode = CollisionDetectionMode.ContinuousSpeculative;
                }

                Instantiate(Resources.Load("Prefabs/Right Hand Palm Capsules"), hand);

                rightHandCollidersHasBeenSet = true;
            }
        }

        // Left hand material
        bool leftActive = false;
        if (avatar.HandLeft != null && avatar.HandLeft.RenderParts.Count > 0)
        {
            if (avatar.HandLeft.RenderParts[0].gameObject.activeSelf)
            {
                leftActive = true;
                if (!leftHandMaterialHasBeenSet)
                {
                    leftHandMaterialHasBeenSet = true;
                    avatar.HandLeft.RenderParts[0].mesh.sharedMaterial = leftHandMaterial;
                }
            }
            else if (leftHandMaterialHasBeenSet)
            {
                leftHandMaterialHasBeenSet = false;
            }
        }
        if (isLeftControllerActive != leftActive) isLeftControllerActive = leftActive;

        // Right hand material
        bool rightActive = false;
        if (avatar.HandRight != null && avatar.HandRight.RenderParts.Count > 0)
        {
            if (avatar.HandRight.RenderParts[0].gameObject.activeSelf)
            {
                rightActive = true;
                if (!rightHandMaterialHasBeenSet)
                {
                    rightHandMaterialHasBeenSet = true;
                    avatar.HandRight.RenderParts[0].mesh.sharedMaterial = rightHandMaterial;
                }
            }
            else if (rightHandMaterialHasBeenSet)
            {
                rightHandMaterialHasBeenSet = false;
            }
        }
        if (isRightControllerActive != rightActive) isRightControllerActive = rightActive;
    }

    public void UpdateLeftHandMaterial(Material material)
    {
        leftHandMaterial = material;
        leftHandMaterialHasBeenSet = false;
    }

    public void UpdateRightHandMaterial(Material material)
    {
        rightHandMaterial = material;
        rightHandMaterialHasBeenSet = false;
    }

    public void SwitchLeftControllerVisualization(VisualizationMode visualizationMode)
    {
        leftVisualizationMode = visualizationMode;
        switch (leftVisualizationMode)
        {
            case VisualizationMode.HandsOnly:
                avatar.HandLeft.enabled = true;
                avatar.ShowLeftController(false);
                break;
            case VisualizationMode.HandsAndControllers:
                avatar.HandLeft.enabled = true;
                avatar.ShowLeftController(true);
                break;
            case VisualizationMode.ControllersOnly:
                avatar.HandLeft.enabled = false;
                avatar.HandLeft.RenderParts[0].mesh.enabled = false;
                avatar.ShowLeftController(true);
                break;
        }
    }

    public void SwitchRightControllerVisualization(VisualizationMode visualizationMode)
    {
        rightVisualizationMode = visualizationMode;
        switch (rightVisualizationMode)
        {
            case VisualizationMode.HandsOnly:
                avatar.HandRight.enabled = true;
                avatar.ShowRightController(false);
                break;
            case VisualizationMode.HandsAndControllers:
                avatar.HandRight.enabled = true;
                avatar.ShowRightController(true);
                break;
            case VisualizationMode.ControllersOnly:
                avatar.HandRight.enabled = false;
                avatar.HandRight.RenderParts[0].mesh.enabled = false;
                avatar.ShowRightController(true);
                break;
        }
    }

    public void SetLeftHandCustomPose(Transform pose)
    {
        avatar.LeftHandCustomPose = pose;
        if (leftVisualizationMode.Equals(VisualizationMode.HandsAndControllers))
        {
            if (pose != null) avatar.ShowLeftController(false);
            else avatar.ShowLeftController(true);
        }
    }

    public void SetRightHandCustomPose(Transform pose)
    {
        avatar.RightHandCustomPose = pose;
        if (rightVisualizationMode.Equals(VisualizationMode.HandsAndControllers))
        {
            if (pose != null) avatar.ShowRightController(false);
            else avatar.ShowRightController(true);
        }
    }
}
