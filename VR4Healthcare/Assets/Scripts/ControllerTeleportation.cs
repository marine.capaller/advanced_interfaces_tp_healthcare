﻿using UnityEngine;

public class ControllerTeleportation : Teleportation
{
    [SerializeField]
    private OVRInput.Button button = default;

    private void Update()
    {
        if (OVRInput.Get(button))
        {
            otherHandTeleportation.enabled = false;
            DrawTrajectory(transform.forward);
        }

        if (OVRInput.GetUp(button))
        {
            if (lastHitIsValid) MovePlayerToLastHitPos();

            ClearTrajectory();
            otherHandTeleportation.enabled = true;
        }
    }
}
