﻿using UnityEngine;

public class Swab : GenericGrabbable
{
    public static int nbUsedSwabsRemaining = 0;

    [SerializeField]
    private Mesh openSwabMesh = null;

    [SerializeField]
    private Material[] openSwabMaterials = null;

    [SerializeField]
    private Color usedColor = new Color(180, 180, 180);

    [SerializeField]
    private Vector3 openSwabBoxColliderSize = Vector3.zero;

    [HideInInspector]
    public bool isUsed = false;

    private OVRGrabbable ovrGrabbable = null;

    private MeshFilter meshFilter = null;

    private MeshRenderer meshRenderer = null;

    private BoxCollider boxCollider = null;

    private bool isOpen = false;

    protected override void Start()
    {
        base.Start();

        ovrGrabbable = GetComponent<OVRGrabbable>();
        meshFilter = GetComponent<MeshFilter>();
        meshRenderer = GetComponent<MeshRenderer>();
        boxCollider = GetComponent<BoxCollider>();

        nbUsedSwabsRemaining = 0;
    }

    private void Update()
    {
        if (!isOpen && ovrGrabbable.isGrabbed)
        {
            isOpen = true;
            meshFilter.mesh = openSwabMesh;
            boxCollider.size = openSwabBoxColliderSize;
            meshRenderer.materials = openSwabMaterials;
        }
    }

    public void Use()
    {
        isUsed = true;
        nbUsedSwabsRemaining++;
        meshRenderer.material.color = usedColor;
    }
}
