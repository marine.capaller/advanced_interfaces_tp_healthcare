﻿using UnityEngine;

public class GenericGrabbable : OVRGrabbable
{
    public Transform leftHandTrackingSnapOffset = default;

    public Transform rightHandTrackingSnapOffset = default;
}
