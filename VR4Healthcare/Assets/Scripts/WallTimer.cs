﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class WallTimer : MonoBehaviour
{
    private Canvas canvas = default;

    private Text uiText = default;

    private float startTime = default;

    private void Start()
    {
        canvas = GetComponent<Canvas>();
        uiText = transform.GetChild(0).GetComponent<Text>();

        canvas.enabled = false;
    }

    private void Update()
    {
        if (GameManager.Instance.isPlaying)
        {
            if (startTime == 0f)
            {
                startTime = Time.time;
                canvas.enabled = true;
            }
            else
            {
                GameManager.Instance.durationSeconds = (int)(Time.time - startTime);
                TimeSpan duration = TimeSpan.FromSeconds(GameManager.Instance.durationSeconds);
                uiText.text = duration.ToString(@"hh\:mm\:ss");
            }
        }
    }
}
