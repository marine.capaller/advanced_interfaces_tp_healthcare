﻿using System;
using UnityEngine;
using UnityEngine.Events;

[Serializable]
public class ColliderTriggerEvent : UnityEvent<Collider, Transform> { }

public class Trigger : MonoBehaviour
{
    [SerializeField]
    private ColliderTriggerEvent onTriggerEnter = null;

    private void OnTriggerEnter(Collider other)
    {
        onTriggerEnter.Invoke(other, transform);
    }
}
