﻿using UnityEngine;

public class SocketGrabbable : GenericGrabbable
{
    [SerializeField]
    private GameObject onDetachObject = null;

    [SerializeField]
    private GameObject onAttachObject = null;

    public void OnAttach()
    {
        if (onAttachObject != null && onDetachObject != null)
        {
            onDetachObject.SetActive(false);
            onAttachObject.SetActive(true);
        }
    }

    public void OnDetach()
    {
        if (onAttachObject != null && onDetachObject != null)
        {
            onAttachObject.SetActive(false);
            onDetachObject.SetActive(true);
        }
    }
}
