﻿using UnityEngine;

public class GelDrop : MonoBehaviour
{
    private void OnCollisionEnter(Collision collision)
    {
        HandManager handManager = null;
        OvrAvatarHand ovrAvatarHand = null;
        Transform obj = collision.gameObject.transform;
        Transform parent = collision.transform.parent;

        while (parent != null)
        {
            handManager = obj.GetComponent<HandManager>();
            ovrAvatarHand = obj.GetComponent<OvrAvatarHand>();
            if (handManager != null || ovrAvatarHand != null) break;

            obj = parent;
            parent = obj.parent;
        }

        if (ovrAvatarHand != null) {
            LocalAvatarManager localAvatarManager = ovrAvatarHand.GetComponentInParent<LocalAvatarManager>();
            handManager = (ovrAvatarHand.isLeftHand) ? localAvatarManager.leftHandManager : localAvatarManager.rightHandManager;
        }

        if (handManager != null) handManager.Disinfect();

        Destroy(gameObject);
    }
}
