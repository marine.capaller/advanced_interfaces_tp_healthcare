﻿using System;
using UnityEngine;

public class Vacutainer : GenericGrabbable
{
    public enum VacutainerType
    {
        Hemostasis,
        BloodCount
    }

    public VacutainerType vacutainerType = default;

    public bool isFillingUp = false;

    [HideInInspector]
    public bool isFilled = false;

    [HideInInspector]
    public Action<bool> onDetachCallback = null;

    [SerializeField]
    private float totalFillingTime = 4f;

    [SerializeField]
    private Material bloodMaterial = null;

    private MeshRenderer meshRenderer = null;

    private Material glassMaterial = null;

    private int[] attachedMatsIndexSeq = new int[8] { 3, 10, 9, 8, 7, 6, 5, 4 };

    private int[] detachedMatsIndexSeq = new int[8] { 1, 4, 5, 6, 7, 8, 9, 10 };

    private float fillingTime = 0f;

    private float stepTime = 0f;

    private int fillingIndex = -1;

    protected override void Start()
    {
        base.Start();

        meshRenderer = GetComponent<MeshRenderer>();
        glassMaterial = meshRenderer.materials[1];

        stepTime = totalFillingTime / attachedMatsIndexSeq.Length;
    }

    private void Update()
    {
        if (isFillingUp)
        {
            fillingTime += Time.deltaTime;

            if (fillingTime >= stepTime)
            {
                fillingTime = 0;
                fillingIndex++;

                Material[] materials = meshRenderer.materials;
                materials[attachedMatsIndexSeq[fillingIndex]] = bloodMaterial;
                meshRenderer.materials = materials;

                if (fillingIndex >= attachedMatsIndexSeq.Length - 1)
                {
                    isFillingUp = false;
                    isFilled = true;
                    allowOffhandGrab = true;
                }
            }
        }
    }

    public override void GrabEnd(Vector3 linearVelocity, Vector3 angularVelocity)
    {
        Rigidbody rb = gameObject.GetComponent<Rigidbody>();
        rb.isKinematic = false;
        rb.velocity = linearVelocity;
        rb.angularVelocity = angularVelocity;
        m_grabbedBy = null;
        m_grabbedCollider = null;
    }

    public void OnAttach()
    {
        Material[] materials = meshRenderer.materials;
        for (int i = 0; i < attachedMatsIndexSeq.Length; i++)
        {
            if (i <= fillingIndex) materials[attachedMatsIndexSeq[i]] = bloodMaterial;
            else materials[attachedMatsIndexSeq[i]] = glassMaterial;
        }
        materials[detachedMatsIndexSeq[0]] = glassMaterial;
        meshRenderer.materials = materials;
    }

    public void OnDetach()
    {
        isFillingUp = false;

        Material[] materials = meshRenderer.materials;
        for (int i = 0; i < detachedMatsIndexSeq.Length; i++)
        {
            if (i <= fillingIndex) materials[detachedMatsIndexSeq[i]] = bloodMaterial;
            else materials[detachedMatsIndexSeq[i]] = glassMaterial;
        }
        materials[attachedMatsIndexSeq[0]] = glassMaterial;
        meshRenderer.materials = materials;
    }
}
