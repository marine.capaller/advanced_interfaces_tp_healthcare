﻿using UnityEngine;

public abstract class Socket : MonoBehaviour
{
    [HideInInspector]
    public GameObject attachedObject = default;

    [SerializeField, TagSelector]
    protected string grabbableTag = default;

    protected GameObject hoveredObject = default;

    virtual protected void Update()
    {
        if (hoveredObject != null)
        {
            // On attach
            if (!hoveredObject.GetComponent<OVRGrabbable>().isGrabbed)
            {
                // Grabbable state changes from hovered to attached
                attachedObject = hoveredObject;
                hoveredObject = null;

                // If attachedObject is a SocketGrabbable, it may have an onAttachObject configured
                SocketGrabbable socketGrabbable = attachedObject.GetComponent<SocketGrabbable>();
                if (socketGrabbable != null) socketGrabbable.OnAttach();

                // Reset position and rotation then freeze
                attachedObject.transform.parent = transform;
                attachedObject.GetComponent<Rigidbody>().isKinematic = true;
                attachedObject.transform.position = transform.position;
                attachedObject.transform.rotation = transform.rotation;

                OnAttach();
            }
        }

        // On detach
        if (attachedObject != null)
        {
            if (attachedObject.GetComponent<OVRGrabbable>() && attachedObject.GetComponent<OVRGrabbable>().isGrabbed)
            {
                // If attachedObject is a SocketGrabbable, it may have an onAttachObject configured
                SocketGrabbable socketGrabbable = attachedObject.GetComponent<SocketGrabbable>();
                if (socketGrabbable != null) socketGrabbable.OnDetach();

                // Grabbable state changes from attached to hovered
                hoveredObject = attachedObject;

                OnDetach();

                attachedObject = null;
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag(grabbableTag) && attachedObject == null && hoveredObject == null)
        {
            hoveredObject = other.gameObject;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag(grabbableTag) && hoveredObject != null)
        {
            hoveredObject = null;
        }
    }

    public abstract void OnAttach();

    public abstract void OnDetach();
}
