﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class VRKeyboard : MonoBehaviour
{
    public InputField targetField = default;

    private void OnEnable()
    {
        EventSystem.current.SetSelectedGameObject(targetField.gameObject, null);
        targetField.OnPointerClick(new PointerEventData(EventSystem.current));
    }

    public void WriteChar(string character)
    {
        targetField.text += character;
        targetField.MoveTextEnd(true);
    }

    public void Backspace()
    {
        string text = targetField.text;
        if (text.Length > 0) targetField.text = text.Substring(0, text.Length - 1);
    }

    public void Clear()
    {
        targetField.text = "";
    }
}
