﻿using UnityEngine;

public class Patient : MonoBehaviour
{
    [HideInInspector]
    public bool isTourniquetAttached = false;

    [HideInInspector]
    public bool isBandageAttached = false;

    [HideInInspector]
    public bool isBleeding = false;

    [HideInInspector]
    public bool isBlood = false;

    [HideInInspector]
    public int disinfectionCounter = 0;

    [HideInInspector]
    public bool animationIsRunning = false;

    [SerializeField]
    private MeshRenderer patientRenderer = null;

    [SerializeField]
    private ParticleSystem bleedingParticleSystem = null;

    [SerializeField]
    private ParticleSystem contaminationParticleSystem = null;

    [SerializeField]
    private Material skin = null;

    [SerializeField]
    private Material transparentSkin = null;

    [SerializeField]
    private Material blood = null;

    [SerializeField]
    private Material transparentBlood = null;

    [SerializeField]
    private Material[] disinfectedSkins = null;

    [SerializeField]
    private Material[] transparentDisinfectedSkins = null;

    [SerializeField]
    private float[] lightnessAnimationFactors = { 0.1f, 0.2f, 0.3f };

    [SerializeField]
    private int nbAnimationWaves = 2;

    [SerializeField]
    private float animationSpeed = 50;

    private bool isNeedleInside = false;

    private bool needleHasBeenInsertedOnce = false;

    private bool needleHasBeenRemovedOnce = false;

    private Color initialColor = new Color();

    private float currentLightnessFactor = 0f;

    private bool isLightnessIncreasing = true;

    private int currentAnimationWave = 0;

    private float animationSpeedFactor = 1f;

    private void Update()
    {
        if (GameManager.Instance.areParticlesVisible && contaminationParticleSystem.isStopped) contaminationParticleSystem.Play();
        else if (!GameManager.Instance.areParticlesVisible && contaminationParticleSystem.isPlaying) contaminationParticleSystem.Stop();

        if (animationIsRunning)
        {
            float speed = animationSpeed * animationSpeedFactor;
            currentLightnessFactor += ((isLightnessIncreasing) ? 0.01f : -0.01f) * speed * Time.deltaTime;

            if (currentLightnessFactor > lightnessAnimationFactors[disinfectionCounter - 1])
            {
                currentLightnessFactor = lightnessAnimationFactors[disinfectionCounter - 1];
                isLightnessIncreasing = false;
            }
            else if (currentLightnessFactor <= 0f && !isLightnessIncreasing)
            {
                currentLightnessFactor = 0;
                isLightnessIncreasing = true;
                currentAnimationWave++;

                if (currentAnimationWave == nbAnimationWaves)
                {
                    currentAnimationWave = 0;
                    animationIsRunning = false;
                }
            }

            Material material = (isTourniquetAttached) ? transparentDisinfectedSkins[disinfectionCounter - 1] : disinfectedSkins[disinfectionCounter - 1];
            Color color = material.color;
            color.r = initialColor.r / (1f - currentLightnessFactor);
            color.g = initialColor.g / (1f - currentLightnessFactor);
            color.b = initialColor.b / (1f - currentLightnessFactor);
            material.color = color;
        }
    }

    public void OnAttachBandage()
    {
        isBandageAttached = true;
        isBleeding = false;
        bleedingParticleSystem.Stop();

        bool bothHandsGloved = true;
        foreach (var handManager in FindObjectsOfType<HandManager>()) if (!handManager.gloved) bothHandsGloved = false;
        if (!bothHandsGloved) GameManager.Instance.achievedGoals["USE_GLOVES_FOR_BLOOD_BODY_FLUIDS"] = false;
        GameManager.Instance.achievedGoals["APPLY_BANDAGE"] = true;
        if (isBlood) GameManager.Instance.achievedGoals["SPONGE_BLOOD_WITH_GAUZE"] = false;
    }

    public void OnAttachTourniquet()
    {
        isTourniquetAttached = true;

        if (!isBandageAttached && needleHasBeenInsertedOnce && !isNeedleInside)
        {
            isBlood = true;
            isBleeding = true;
            bleedingParticleSystem.Play();
        }

        Material[] materials = patientRenderer.materials;
        materials[1] = transparentSkin;
        if (disinfectionCounter == 0)
        {
            materials[2] = needleHasBeenRemovedOnce ? transparentBlood : transparentSkin;
            materials[3] = needleHasBeenInsertedOnce ? transparentBlood : transparentSkin;
        }
        else
        {
            materials[2] = needleHasBeenRemovedOnce ? transparentBlood : transparentDisinfectedSkins[disinfectionCounter - 1];
            materials[3] = needleHasBeenInsertedOnce ? transparentBlood : transparentDisinfectedSkins[disinfectionCounter - 1];
        }
        patientRenderer.materials = materials;
    }

    public void OnDetachTourniquet()
    {
        isTourniquetAttached = false;

        isBleeding = false;
        bleedingParticleSystem.Stop();

        Material[] materials = patientRenderer.materials;
        materials[1] = skin;
        if (disinfectionCounter == 0)
        {
            materials[2] = needleHasBeenRemovedOnce ? blood : skin;
            materials[3] = needleHasBeenInsertedOnce ? blood : skin;
        }
        else
        {
            materials[2] = needleHasBeenRemovedOnce ? blood : disinfectedSkins[disinfectionCounter - 1];
            materials[3] = needleHasBeenInsertedOnce ? blood : disinfectedSkins[disinfectionCounter - 1];
        }
        patientRenderer.materials = materials;
    }

    public void OnDisinfectArea()
    {
        Material material = isTourniquetAttached ? transparentDisinfectedSkins[disinfectionCounter - 1] : disinfectedSkins[disinfectionCounter - 1];
        Material[] materials = patientRenderer.materials;
        materials[2] = needleHasBeenRemovedOnce ? isTourniquetAttached ? transparentBlood : blood : material;
        materials[3] = needleHasBeenInsertedOnce ? isTourniquetAttached ? transparentBlood : blood : material;
        patientRenderer.materials = materials;
        initialColor = material.color;
        
        if (disinfectionCounter > 1) animationSpeedFactor = lightnessAnimationFactors[disinfectionCounter - 1] / lightnessAnimationFactors[0];
        animationIsRunning = true;
    }

    public void OnAbsorBlood()
    {
        if (isBleeding) return;
        isBlood = false;
        Material[] materials = patientRenderer.materials;
        materials[2] = isTourniquetAttached ? transparentSkin : skin;
        patientRenderer.materials = materials;

        bool bothHandsGloved = true;
        foreach (var handManager in FindObjectsOfType<HandManager>()) if (!handManager.gloved) bothHandsGloved = false;
        if (!bothHandsGloved) GameManager.Instance.achievedGoals["USE_GLOVES_FOR_BLOOD_BODY_FLUIDS"] = false;
        GameManager.Instance.achievedGoals["SPONGE_BLOOD_WITH_GAUZE"] = true;
    }



    public void OnNeedleInserted()
    {
        isNeedleInside = true;
        isBleeding = false;
        bleedingParticleSystem.Stop();

        bool bothHandsGloved = true;
        foreach (var handManager in FindObjectsOfType<HandManager>()) if (!handManager.gloved) bothHandsGloved = false;
        if (!bothHandsGloved) GameManager.Instance.achievedGoals["USE_GLOVES_FOR_BLOOD_BODY_FLUIDS"] = false;
        if (!isTourniquetAttached) GameManager.Instance.achievedGoals["NEEDLE_INSERTED_WITH_TOURNIQUET"] = false;

        if (!needleHasBeenInsertedOnce)
        {
            needleHasBeenInsertedOnce = true;

            Material[] materials = patientRenderer.materials;
            if (isTourniquetAttached) materials[3] = transparentBlood;
            else materials[3] = blood;
            patientRenderer.materials = materials;
        }
    }

    public void OnNeedleRemoved()
    {
        isNeedleInside = false;
        isBlood = true;

        if (!needleHasBeenRemovedOnce) needleHasBeenRemovedOnce = true;

        Material[] materials = patientRenderer.materials;
        if (isTourniquetAttached)
        {
            materials[2] = transparentBlood;

            isBleeding = true;
            bleedingParticleSystem.Play();
        }
        else
        {
            materials[2] = blood;
        }
        patientRenderer.materials = materials;
    }
}
