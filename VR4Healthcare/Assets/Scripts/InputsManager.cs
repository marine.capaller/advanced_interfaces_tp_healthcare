﻿using UnityEngine;
using UnityEngine.SceneManagement;
using static LocalAvatarManager;

public class InputsManager : MonoBehaviour
{
    [SerializeField]
    private LocalAvatarManager localAvatarManager = default;

    [SerializeField]
    private bool joystickLocomotion = default;

    [SerializeField]
    private float speed = 2.0f;

    [SerializeField]
    private bool snapTurn = default;

    [SerializeField]
    private float rotationAngle = 45.0f;

    private OVRCameraRig cameraRig = default;

    private bool readyToSnapTurn = default;

    private Vector3 cameraRigInitialPos = default;

    private void Start()
    {
        cameraRig = GetComponent<OVRCameraRig>();
        cameraRigInitialPos = cameraRig.transform.position;

        OVRManager.display.RecenteredPose += OnRecenteredEvent;
    }

    private void Update()
    {
        if (OVRInput.GetDown(OVRInput.Button.Start))
        {
            OVRManager.display.RecenteredPose -= OnRecenteredEvent;
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }

        if (joystickLocomotion)
        {
            Vector2 primaryAxis = OVRInput.Get(OVRInput.Axis2D.PrimaryThumbstick);
            if (!primaryAxis.Equals(Vector2.zero))
            {
                Quaternion ort = cameraRig.centerEyeAnchor.rotation;
                Vector3 ortEuler = ort.eulerAngles;
                ortEuler.z = ortEuler.x = 0f;
                ort = Quaternion.Euler(ortEuler);

                Vector3 moveDir = Vector3.zero;
                moveDir += ort * (primaryAxis.x * Vector3.right);
                moveDir += ort * (primaryAxis.y * Vector3.forward);

                transform.Translate(moveDir * speed * Time.deltaTime, Space.World);
            }
        }

        if (snapTurn)
        {
            if (OVRInput.Get(OVRInput.Button.SecondaryThumbstickLeft))
            {
                if (readyToSnapTurn)
                {
                    Vector3 euler = transform.rotation.eulerAngles;
                    euler.y -= rotationAngle;
                    transform.rotation = Quaternion.Euler(euler);
                    readyToSnapTurn = false;
                }
            }
            else if (OVRInput.Get(OVRInput.Button.SecondaryThumbstickRight))
            {
                if (readyToSnapTurn)
                {
                    Vector3 euler = transform.rotation.eulerAngles;
                    euler.y += rotationAngle;
                    transform.rotation = Quaternion.Euler(euler);
                    readyToSnapTurn = false;
                }
            }
            else
            {
                readyToSnapTurn = true;
            }
        }

        if (OVRInput.GetDown(OVRInput.Button.Two, OVRInput.Controller.LTouch))
        {
            if (localAvatarManager.leftVisualizationMode.Equals(VisualizationMode.HandsOnly)) localAvatarManager.SwitchLeftControllerVisualization(VisualizationMode.HandsAndControllers);
            else if (localAvatarManager.leftVisualizationMode.Equals(VisualizationMode.HandsAndControllers)) localAvatarManager.SwitchLeftControllerVisualization(VisualizationMode.HandsOnly);
        }

        if (OVRInput.GetDown(OVRInput.Button.Two, OVRInput.Controller.RTouch))
        {
            if (localAvatarManager.rightVisualizationMode.Equals(VisualizationMode.HandsOnly)) localAvatarManager.SwitchRightControllerVisualization(VisualizationMode.HandsAndControllers);
            else if (localAvatarManager.rightVisualizationMode.Equals(VisualizationMode.HandsAndControllers)) localAvatarManager.SwitchRightControllerVisualization(VisualizationMode.HandsOnly);
        }
    }

    private void OnRecenteredEvent()
    {
        cameraRig.transform.position = cameraRigInitialPos;
    }
}
