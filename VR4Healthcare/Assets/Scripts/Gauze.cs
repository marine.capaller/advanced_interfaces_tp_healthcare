﻿using UnityEngine;

public class Gauze : GenericGrabbable
{
    public static int nbUsedGauzesRemaining = 0;

    [SerializeField]
    private Color usedColor = new Color(255, 208, 208);

    [HideInInspector]
    public bool isUsed = false;

    private MeshRenderer meshRenderer = null;

    protected override void Start()
    {
        base.Start();

        meshRenderer = GetComponent<MeshRenderer>();

        nbUsedGauzesRemaining = 0;
    }

    public override void GrabEnd(Vector3 linearVelocity, Vector3 angularVelocity)
    {
        Rigidbody rb = gameObject.GetComponent<Rigidbody>();
        rb.isKinematic = false;
        rb.velocity = linearVelocity;
        rb.angularVelocity = angularVelocity;
        m_grabbedBy = null;
        m_grabbedCollider = null;
    }

    public void Use()
    {
        isUsed = true;
        nbUsedGauzesRemaining++;
        meshRenderer.material.color = usedColor;
    }
}
