﻿using UnityEngine;

public class DoorHandle : VRInteractable
{
    [SerializeField]
    private GameObject endPanel = default;

    [SerializeField]
    private GameObject errorPanel = default;

    [SerializeField]
    private GameObject sucessLabels = default;

    [SerializeField]
    private GameObject errorLabels = default;

    public override void OnGrab(GameObject hand)
    {
        if (GameManager.Instance.isPlaying)
        {
            if (GameManager.Instance.readyToExitRoom)
            {
                GameManager.Instance.ExitRoom(delegate (bool success)
                {
                    errorPanel.SetActive(false);
                    endPanel.SetActive(true);
                    if (!success)
                    {
                        sucessLabels.SetActive(false);
                        errorLabels.SetActive(true);
                    }
                    enabled = false;
                });
            }
            else
            {
                errorPanel.SetActive(true);
            }
        }
    }
}
