﻿using UnityEngine;

public class Cupboard : MonoBehaviour
{
    [SerializeField]
    private Canvas openedCanvas = null;

    [SerializeField]
    private Canvas closedCanvas = null;

    [SerializeField]
    private AudioClip cupboardDoorSound = null;

    private Animator animator = null;

    private AudioSource audioSource = null;

    private bool doorOpened = false;

    private bool animationIsRunning = false;

    private void Start()
    {
        animator = GetComponent<Animator>();
        audioSource = GetComponent<AudioSource>();

        openedCanvas.GetComponent<TextIndicator>().manualMode = true;
    }

    public void OpenClose()
    {
        if (!animationIsRunning)
        {
            audioSource.PlayOneShot(cupboardDoorSound);
            animationIsRunning = true;
            doorOpened = !doorOpened;
            if (doorOpened)
            {
                closedCanvas.GetComponent<TextIndicator>().manualMode = true;
                closedCanvas.enabled = false;
            }
            else
            {
                openedCanvas.GetComponent<TextIndicator>().manualMode = true;
                openedCanvas.enabled = false;
            }
            animator.SetBool("opened", doorOpened);
        }
    }

    public void OnAnimationExit()
    {
        if (doorOpened)
        {
            openedCanvas.enabled = true;
            openedCanvas.GetComponent<TextIndicator>().manualMode = false;
        }
        else
        {
            closedCanvas.enabled = true;
            closedCanvas.GetComponent<TextIndicator>().manualMode = false;
        }
        animationIsRunning = false;
    }
}
