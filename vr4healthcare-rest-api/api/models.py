from django.contrib.auth.base_user import BaseUserManager
from django.contrib.auth.models import AbstractUser
from django.db import models
from django.utils import formats, timezone
from django.utils.translation import ugettext_lazy as _


class UserManager(BaseUserManager):
    use_in_migrations = True

    def create_user(self, username, password, is_staff):
        user = self.model(username=username, is_staff=is_staff)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, username, password):
        user = self.create_user(username=username, password=password, is_staff=True)
        user.is_superuser = True
        user.save(using=self._db)
        return user


class User(AbstractUser):
    objects = UserManager()

    def has_usable_password(self):
        return self.is_superuser


class Scenario(models.Model):
    identifier = models.CharField(max_length=64, unique=True, verbose_name=_('identifier'))
    name = models.CharField(max_length=64, verbose_name=_('name'))
    goals = models.ManyToManyField('Goal', blank=True, verbose_name=_('goals'))

    class Meta:
        verbose_name = _('scenario')
        verbose_name_plural = _('scenarios')

    def __str__(self):
        return self.name


class Game(models.Model):
    EASY = 0
    NORMAL = 1
    DIFFICULTY_CHOICES = (
        (EASY, _('Easy')),
        (NORMAL, _('Normal'))
    )

    player_name = models.CharField(max_length=64, verbose_name=_('player name'))
    datetime = models.DateTimeField(verbose_name=_('date and time'))
    duration = models.DurationField(verbose_name=_('duration'))
    score = models.IntegerField(verbose_name=_('score'))
    difficulty = models.IntegerField(choices=DIFFICULTY_CHOICES, default=EASY, verbose_name=_("difficulty"))
    scenario = models.ForeignKey('Scenario', models.CASCADE, verbose_name=_('scenario'))
    achieved_goals = models.ManyToManyField('Goal', through='GameAchievedGoal', verbose_name=_('goals'))

    class Meta:
        verbose_name = _('game')
        verbose_name_plural = _('games')

    def __str__(self):
        return '[' + formats.date_format(timezone.localtime(self.datetime),
                                         'DATETIME_FORMAT') + '] ' + self.player_name + ' (' + str(self.score) + ')'


class Goal(models.Model):
    identifier = models.CharField(max_length=64, unique=True, verbose_name=_('identifier'))
    description = models.TextField(max_length=256, verbose_name=_('description'))

    class Meta:
        verbose_name = _('goal')
        verbose_name_plural = _('goals')

    def __str__(self):
        return self.identifier


class GameAchievedGoal(models.Model):
    achieved = models.BooleanField(verbose_name=_('achieved'))
    game = models.ForeignKey('Game', on_delete=models.CASCADE, verbose_name=_('game'))
    goal = models.ForeignKey('Goal', on_delete=models.CASCADE, verbose_name=_('goal'))

    class Meta:
        verbose_name = _('goal')
        verbose_name_plural = _('goals')

    def __str__(self):
        return str(self.goal.id)
