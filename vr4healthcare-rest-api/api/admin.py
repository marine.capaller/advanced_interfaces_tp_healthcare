from django.contrib import admin
from django.contrib.auth.models import Group
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from django.utils.translation import ugettext_lazy as _

from api.models import Game, Scenario, Goal, User, GameAchievedGoal

admin.site.site_title = 'VR4Healthcare'
admin.site.site_header = 'VR4Healthcare'


class GameAchievedGoalsInline(admin.TabularInline):
    model = Game.achieved_goals.through
    after_field = 'Goal'
    readonly_fields = ('goal_description',)
    extra = 1

    def goal_description(self, obj):
        return obj.goal.description

    goal_description.short_description = _('description')

    def get_fields(self, request, obj=None):
        if request.user.is_superuser:
            return 'goal', 'achieved'
        else:
            return 'achieved', 'goal_description'

    def has_module_permission(self, request):
        return request.user.is_staff

    def has_view_permission(self, request, obj=None):
        return request.user.is_staff


class GameAdmin(admin.ModelAdmin):
    inlines = (GameAchievedGoalsInline,)
    list_display = ('player_name', 'scenario', 'difficulty', 'datetime', 'score')
    list_filter = ('scenario__name', 'difficulty', 'player_name')
    ordering = ['-datetime']

    def has_module_permission(self, request):
        return request.user.is_staff

    def has_view_permission(self, request, obj=None):
        return request.user.is_staff


class UserAdmin(BaseUserAdmin):
    fieldsets = (
        (None, {'fields': ('username', 'password', 'is_active', 'is_staff', 'is_superuser')}),
    )

    add_fieldsets = (
        (
            None,
            {
                'classes': ('wide',),
                'fields': ('username', 'password1', 'password2')
            }
        ),
    )

    list_display = ('__str__',)


admin.site.unregister(Group)
admin.site.register(Game, GameAdmin)
admin.site.register(Scenario)
admin.site.register(Goal)
admin.site.register(User, UserAdmin)
