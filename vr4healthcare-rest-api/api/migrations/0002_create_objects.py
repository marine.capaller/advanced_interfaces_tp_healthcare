from datetime import datetime, timedelta

from django.db import migrations
from api.models import User, Scenario, Goal, Game


def create_objects(apps, schema_editor):
    # Superuser
    superuser = User.objects.create_superuser(username='admin', password='admin')
    superuser.save()

    # Users
    students = User.objects.create_user(username='students', password='students', is_staff=True)
    students.save()

    # Scenario
    scenario = Scenario(identifier='BLOOD_TEST', name='Prélèvement sanguin')
    scenario.save()

    # Goals
    goal1 = Goal(
        identifier='DISINFECT_3_TIMES_BEFORE_PUNCTURE',
        description='Désinfecter 3 fois le site de ponction avant de faire le prélèvement.'
    )
    goal1.save()
    goal2 = Goal(
        identifier='WAIT_30_SECONDS_AFTER_DISINFECTION',
        description='Attendre 30 secondes après avoir désinfecté le site de ponction pour que la Chlorhexidine puisse faire son effet.'
    )
    goal2.save()
    goal3 = Goal(
        identifier='NEEDLE_INSERTED_WITH_TOURNIQUET',
        description='Introduire l\'aiguille avec le garrot attaché.'
    )
    goal3.save()
    goal4 = Goal(
        identifier='BUTTERFLY_ATTACHED_FIRST_TRY',
        description='Fixer le système butterfly au premier essai.'
    )
    goal4.save()
    goal5 = Goal(
        identifier='HEMOSTASIS_BEFORE_BLOOD_COUNT',
        description='Remplir le vacutainer pour l\'hémostase avant celui pour la formule sanguine réduite.'
    )
    goal5.save()
    goal6 = Goal(
        identifier='NEEDLE_REMOVED_WITHOUT_TOURNIQUET',
        description='Retirer l\'aiguille sans le garrot attaché afin d\'éviter les saignements.'
    )
    goal6.save()
    goal7 = Goal(
        identifier='SPONGE_BLOOD_WITH_GAUZE',
        description='Appliquer une pression sur le site de ponction avec un coton après avoir retiré l\'aiguille afin d\'éviter les hématomes et les saignements.'
    )
    goal7.save()
    goal8 = Goal(
        identifier='APPLY_BANDAGE',
        description='Placer le pansement.'
    )
    goal8.save()
    goal9 = Goal(
        identifier='DISINFECTED_HANDS_BEFORE_GLOVING',
        description='Se désinfecter les mains avant de mettre des gants.'
    )
    goal9.save()
    goal10 = Goal(
        identifier='GLOVES_IN_YELLOW_TRASH',
        description='Jeter les gants usagés dans la poubelle jaune*.\n\n*Dans la réalité, les gants qui n\'ont pas été en contact avec du sang ou des fluides corporels doivent être jetés dans la poubelle noire.'
    )
    goal10.save()
    goal11 = Goal(
        identifier='SWABS_IN_BLACK_TRASH',
        description='Jeter les tampons antiseptiques usagés dans la poubelle noire.'
    )
    goal11.save()
    goal12 = Goal(
        identifier='GAUZES_IN_YELLOW_TRASH',
        description='Jeter les cotons usagés dans la poubelle jaune.'
    )
    goal12.save()
    goal13 = Goal(
        identifier='BUTTERFLY_IN_SAFETY_BOX',
        description='Jeter le système butterfly dans la SAFE-BOX.'
    )
    goal13.save()
    goal14 = Goal(
        identifier='PATIENT_NOT_INFECTED',
        description='Ne pas contaminer le patient avec les germes du joueur.'
    )
    goal14.save()
    goal15 = Goal(
        identifier='CUPBOARD_NOT_INFECTED',
        description='Ne pas contaminer l\'armoire de stockage.'
    )
    goal15.save()
    goal16 = Goal(
        identifier='USE_GLOVES_FOR_BLOOD_BODY_FLUIDS',
        description='Utiliser des gants lors de contact avec du sang ou avec des fluides corporels.'
    )
    goal16.save()

    # Assign goals to scenario
    scenario.goals.add(goal1, goal2, goal3, goal4, goal5, goal6, goal7, goal8, goal9, goal10, goal11, goal12, goal13,
                       goal14, goal15, goal16)
    scenario.save()


class Migration(migrations.Migration):
    dependencies = [('api', '0001_initial')]

    operations = [migrations.RunPython(create_objects)]
