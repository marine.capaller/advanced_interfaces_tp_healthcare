from datetime import timedelta

from dateutil import parser
from rest_framework import mixins, viewsets, status
from rest_framework.response import Response

from api.models import Game, Scenario, GameAchievedGoal, Goal


class CheckViewSet(mixins.RetrieveModelMixin, viewsets.GenericViewSet):
    def retrieve(self, request):
        return Response()


class NewGameViewSet(mixins.CreateModelMixin, viewsets.GenericViewSet):
    def create(self, request):
        data = {'detail': 'Game successfully stored'}
        sts = status.HTTP_200_OK
        game = None
        try:
            scenario = Scenario.objects.get(identifier=request.data['scenario'])
            game = Game(
                player_name=request.data['player_name'],
                datetime=parser.parse(request.data['datetime']),
                duration=timedelta(seconds=request.data['duration']),
                score=request.data['score'],
                difficulty=request.data['difficulty'],
                scenario=scenario
            )
            game.save()
            for goal in request.data['achieved_goals']:
                achieved_goal = GameAchievedGoal(
                    achieved=request.data['achieved_goals'][goal],
                    game=game,
                    goal=Goal.objects.get(identifier=goal)
                )
                achieved_goal.save()
        except:
            if game is not None:
                game.delete()
            data = {'detail': 'Something went wrong'}
            sts = status.HTTP_400_BAD_REQUEST

        return Response(data, sts)
