from django.urls import path

from api.views import NewGameViewSet, CheckViewSet

urlpatterns = [
    path('check/', CheckViewSet.as_view({'get': 'retrieve'}), name='check'),
    path('new-game/', NewGameViewSet.as_view({'post': 'create'}), name='new-game')
]
